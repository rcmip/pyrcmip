#!/bin/bash
#
# Release a set of submission templates
#
# If (any) of the templates change then a new template release is needed.
# Update the VERSION variable here and in ./check-templates.sh before running
#

ROOT_DIR=$(dirname "$0")/..
TEST_DATA_DIR=$ROOT_DIR/tests/data
BUCKET=s3://rcmip-protocols-au

VERSION=v5.1.0

aws configure set default.s3.multipart_threshold 128MB
aws configure set default.s3.multipart_chunksize 128MB


function upload {
  FNAME=$1
  # e.g. rcmip-data-submission-template.xlsx => rcmip-data-submission-template-v5-0-0.xlsx
  OUT_FNAME=${FNAME%%.*}-${VERSION//./-}.${FNAME##*.}

  echo "Uploading $FNAME as $OUT_FNAME"

  aws s3 cp $TEST_DATA_DIR/$FNAME $BUCKET/$VERSION/$OUT_FNAME
}

# Template
upload rcmip-data-submission-template.xlsx

# Example model
upload rcmip-data-submission-template-test.xlsx

# Example model output
upload rcmip_model_output_test.xlsx
upload rcmip_model_output_test.csv
upload rcmip_model_output_test.nc

# Example model metadata
upload rcmip-model-meta-test.csv
upload rcmip_model_metadata_test.csv

# Example reported metrics
upload rcmip_model_reported_metrics_test.csv
