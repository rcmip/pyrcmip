#!/bin/bash
#
# Check that the current version of the templates matches those in S3
# This is run as part of the CI process
#

ROOT_DIR=$(dirname "$0")/..
TEST_DATA_DIR=$ROOT_DIR/tests/data
BUCKET=rcmip-protocols-au
HASH_FILE=/tmp/hashes.txt

VERSION=v5.1.0


function fetch_hash {
  FNAME=$1

  # Get the MD5 sum from AWS
  REMOTE_FNAME=${FNAME%%.*}-${VERSION//./-}.${FNAME##*.}
  REMOTE_MD5=$(aws s3api head-object --bucket $BUCKET --key $VERSION/${REMOTE_FNAME} | jq -r .ETag | tr -d '"')

  echo "$REMOTE_MD5 $TEST_DATA_DIR/$FNAME" >> $HASH_FILE
}


rm -f $HASH_FILE

# Template
fetch_hash rcmip-data-submission-template.xlsx

# Example model
fetch_hash rcmip-data-submission-template-test.xlsx

# Example model output
fetch_hash rcmip_model_output_test.xlsx
fetch_hash rcmip_model_output_test.csv
# Varies with different scmdata versions so turn off
# fetch_hash rcmip_model_output_test.nc

# Example model metadata
fetch_hash rcmip-model-meta-test.csv
fetch_hash rcmip_model_metadata_test.csv

# Example reported metrics
fetch_hash rcmip_model_reported_metrics_test.csv

cat $HASH_FILE
md5sum --check $HASH_FILE
