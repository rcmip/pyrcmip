#!/bin/env python
"""
Script for creating new users who have permission to upload files

The script creates a new AWS IAM user and adds them to the rcmip group. This group has WRITE access to an S3 bucket.
An access key is then created and stored in a JWT. This token should then be provided to the user and used when calling
the `rcmip upload`

Existing tokens can be rotated (deleting the old token and creating a new) by providing the --no-create-user option

Examples
--------
`python scripts/create-token.py --secret test --org Uom --name jared.lewis`
"""
import boto3
import click
import jwt

client = boto3.client('iam')


@click.command()
@click.option('--secret', required=True, help='Secret string used to sign JWT.')
@click.option('--group', default="rcmipUsers", help='AWS Group ARN to add the users to')
@click.option('--name', required=True, help='The case-insensitive name for the new user. Of the form {first}.{last}. '
                                 'For example: jared.lewis')
@click.option('--org', required=True, help='Shorthand name of the organisation of the user')
@click.option('--create-user/--no-create-user', default=True, help='Create a new user account')
def add_user(secret, group, name, org, create_user):
    """
    Creates a new user and returns a token allowing them to upload model data to S3
    """
    username = "{}-{}".format(org, name)
    if create_user:
        response = client.create_user(
            Path='/rcmip/',
            UserName=username,
            Tags=[
                {
                    'Key': 'project',
                    'Value': 'rcmip'
                },
                {
                    'Key': 'organisation',
                    'Value': org
                },
            ]
        )
        arn = response["User"]["Arn"]
        click.echo("Created new user with ARN: {}".format(arn))

        client.add_user_to_group(UserName=username, GroupName=group)

    existing_keys = client.list_access_keys(UserName="pnnl-kalyn.dorheim")["AccessKeyMetadata"]

    for k in existing_keys:
        click.echo("Deleting existing access key".format(k["AccessKeyId"]))
        client.delete_access_key(AccessKeyId=k["AccessKeyId"], UserName=username)

    resp = client.create_access_key(UserName=username)
    access_key_id = resp["AccessKey"]["AccessKeyId"]
    secret_access_key = resp["AccessKey"]["SecretAccessKey"]

    token = jwt.encode(
        {
            "access_key_id": access_key_id,
            "secret_access_key": secret_access_key,
            "org": org
        },
        secret,
        algorithm='HS256'
    )

    click.echo("Send the following token to the user: " + token.decode("ascii"))


if __name__ == '__main__':
    add_user(auto_envvar_prefix="RCMIP")
