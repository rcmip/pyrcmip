#!/bin/bash
#
# Check zenodo records against the S3 bucket
#

# update zenodo deposition ID if version changes
ZENODO_DEPOSITION_ID=4589756
PROTOCOL_METADATA_FILE="rcmip-protocol-metadata.json"
BUCKET=rcmip-protocols-au

# read version out of metadata
VERSION=$(jq -r .metadata.version "${PROTOCOL_METADATA_FILE}")
echo "version: ${VERSION}"


ZENODO_FILES=$(curl "https://zenodo.org/api/records/${ZENODO_DEPOSITION_ID}" | jq -c .files[])

for file in $(echo "${ZENODO_FILES}")
do
    FILENAME=$(jq -r '.key' <<< "${file}")
    echo $FILENAME

    ZENODO_MD5=$(jq -r '.checksum' <<< "${file}")
    ZENODO_MD5=${ZENODO_MD5/md5:/}
    echo "$ZENODO_MD5"


    S3_MD5=$(aws s3api head-object --bucket $BUCKET --key ${VERSION}/${FILENAME} | jq -r .ETag | tr -d '"')
    echo $S3_MD5
    if [ "${ZENODO_MD5}" != "${S3_MD5}" ]
    then
        echo "Mismatch: ${VERSION}/${FILENAME}"
        echo "Zenodo: ${ZENODO_MD5}"
        echo "S3: ${S3_MD5}"
        exit 1
    else
        echo "OK"
    fi
done
