#!/bin/bash
#
# Upload a new version of the protocol to Zenodo.
# The protocol is first downloaded from S3, so must be uploaded there
# first.
#

if [ -f .env ]
then
  # thank you https://stackoverflow.com/a/20909045
  echo "Reading .env file"
  export $(grep -v '^#' .env | xargs)
fi

ROOT_DIR=$(dirname "$0")/..
SRC_DATA_DIR=$ROOT_DIR/src/pyrcmip/data
PROTOCOL_DIR=/tmp/rcmip-protocol
PROTOCOL_METADATA_FILE="rcmip-protocol-metadata.json"
DEPOSITION_ID_OLD="4589727"

# read version out of metadata
VERSION=$(jq -r .metadata.version "${PROTOCOL_METADATA_FILE}")
echo "version: ${VERSION}"

DEPOSITION_ID=$(openscm-zenodo create-new-version "${DEPOSITION_ID_OLD}" "${PROTOCOL_METADATA_FILE}" --zenodo-url zenodo.org)

rm -rf "${PROTOCOL_DIR}"
mkdir -p "${PROTOCOL_DIR}"

function prepare_version {
    FNAME=$1

    FNAME_PROTOCOL=${FNAME%%.*}-${VERSION//./-}.${FNAME##*.}
    cp -v "${SRC_DATA_DIR}/$FNAME_PROTOCOL" "${PROTOCOL_DIR}/${FNAME_PROTOCOL}"
}

prepare_version rcmip-data-submission-template.xlsx

function download_from_s3 {
    FNAME=$1

    FNAME_S3=${FNAME%%.*}-${VERSION//./-}.${FNAME##*.}

    URL="https://rcmip-protocols-au.s3-ap-southeast-2.amazonaws.com"
    OUT_FILE="${PROTOCOL_DIR}/${FNAME_S3}"
    S3_FILE="${URL}/${VERSION}/${FNAME_S3}"

    curl "${S3_FILE}" -o "${OUT_FILE}"
}

download_from_s3 rcmip-emissions-annual-means.csv
download_from_s3 rcmip-concentrations-annual-means.csv
download_from_s3 rcmip-radiative-forcing-annual-means.csv

BUCKET_ID=$(openscm-zenodo get-bucket ${DEPOSITION_ID} --zenodo-url zenodo.org)

for f in "${PROTOCOL_DIR}/"*
do
    openscm-zenodo upload "${f}" "${BUCKET_ID}" --zenodo-url zenodo.org
    md5sum "${f}"
done
