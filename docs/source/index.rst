.. pyrcmip documentation master file, created by
   sphinx-quickstart on Mon Jul 27 14:23:36 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pyrcmip
=======

.. include:: ../../README.rst
    :start-after: sec-begin-index
    :end-before: sec-end-index

.. include:: ../../README.rst
    :start-after: sec-begin-license
    :end-before: sec-end-license

If you make use of pyrcmip or any of the RCMIP project, please cite Nicholls et al., *GMDD* 2020 :cite:`rcmip_phase_1_2020`.

References
----------

.. bibliography::
   :style: plain
   :all:

.. toctree::
   :maxdepth: 2
   :caption: Documentation

   installation
   submitting_results
   development

.. toctree::
   :maxdepth: 2
   :caption: API reference

   assessed_ranges
   cli
   database
   errors
   io
   metric_calculations
   plotting
   stats
   validate

.. toctree::
    :maxdepth: 2
    :caption: Versions

    changelog


Index
-----

- :ref:`genindex`
- :ref:`modindex`
- :ref:`search`
