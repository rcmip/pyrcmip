.. _cli-reference:

Command-line interface
----------------------

.. click:: pyrcmip.cli:cli
   :prog: rcmip
   :show-nested:
