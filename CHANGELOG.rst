Changelog
=========

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_, and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

The changes listed in this file are categorised as follows:

    - Added: new features
    - Changed: changes in existing functionality
    - Deprecated: soon-to-be removed features
    - Removed: now removed features
    - Fixed: any bug fixes
    - Security: in case of vulnerabilities.

master
------

Added
~~~~~

- (`!32 <https://gitlab.com/rcmip/pyrcmip/merge_requests/32>`_) :func:`pyrcmip.stats.sample_multivariate_skewed_normal` to allow sampling of a multi-variate skewed normal distribution

v0.5.2 - 2022-02-08
-------------------

Changed
~~~~~~~

- (`!31 <https://gitlab.com/rcmip/pyrcmip/merge_requests/31>`_) Move ``docutils`` to the ``docs`` extra requirements as it is only needed for building the documentation


v0.5.1 - 2021-08-18
-------------------

Added
~~~~~

- (`!30 <https://gitlab.com/rcmip/pyrcmip/merge_requests/30>`_) :class:`pyrcmip.metric_calculations.CalculatorAirborneFraction18501920` and :class:`pyrcmip.metric_calculations.CalculatorAirborneFraction18501990` for calculating airboren fraction
- (`!28 <https://gitlab.com/rcmip/pyrcmip/merge_requests/28>`_) Scripts for uploading the RCMIP protocol to Zenodo

Changed
~~~~~~~

- (`!29 <https://gitlab.com/rcmip/pyrcmip/merge_requests/29>`_) Compare the ETag value from S3 against the md5sum from zenodo for the protocol data. This requires the protocol data to be uploaded as a single part as for multipart uploads the Etag != md5sum of the uploaded file

v0.5.0 - 2021-02-23
-------------------

Changed
~~~~~~~

- (`!27 <https://gitlab.com/rcmip/pyrcmip/merge_requests/27>`_) Use ``openpyxl`` rather than ``xlrd`` as excel engine
- (`!27 <https://gitlab.com/rcmip/pyrcmip/merge_requests/27>`_) Upgrade to ``pyjwt>=2``
- (`!27 <https://gitlab.com/rcmip/pyrcmip/merge_requests/27>`_) Upgrade to ``scmdata>=0.7.3``

Fixed
~~~~~

- (`!26 <https://gitlab.com/rcmip/pyrcmip/merge_requests/26>`_) Remove rogue cells in data submission template (new template released as v5-1-0)

v0.4.1 - 2020-09-14
-------------------

Fixed
~~~~~

- (`!25 <https://gitlab.com/rcmip/pyrcmip/merge_requests/25>`_) Usage of old seaborn API in plotting and broken unit check

v0.4.0 - 2020-09-13
-------------------

Added
~~~~~

- (`!23 <https://gitlab.com/rcmip/pyrcmip/merge_requests/23>`_) Documentation and tests for ``pyrcmip.assessed_ranges`` and ``pyrcmip.metric_calculations``
- (`!22 <https://gitlab.com/rcmip/pyrcmip/merge_requests/22>`_) Add support for downloading submitted data

Changed
~~~~~~~

- (`!23 <https://gitlab.com/rcmip/pyrcmip/merge_requests/23>`_) :meth:`pyrcmip.database.Database.load_data` now requires a ``climate_model`` argument
- (`!23 <https://gitlab.com/rcmip/pyrcmip/merge_requests/23>`_) :meth:`pyrcmip.database.Database.save_summary_table` now expects an ``"RCMIP name"`` column, rather than ``"metric"``
- (`!23 <https://gitlab.com/rcmip/pyrcmip/merge_requests/23>`_) Metric calculations now use the :class:`pyrcmip.metric_calculations.base.Calculator`
- (`!24 <https://gitlab.com/rcmip/pyrcmip/merge_requests/24>`_) Pin test dependency ``moto==1.3.14``
- (`!21 <https://gitlab.com/rcmip/pyrcmip/merge_requests/21>`_) Timeseries submissions must include an ``ensemble_member`` column

Removed
~~~~~~~

- (`!23 <https://gitlab.com/rcmip/pyrcmip/merge_requests/23>`_) :func:`pyrcmip.database.time_mean`

v0.3.0 - 2020-09-02
-------------------

Added
~~~~~

- (`!19 <https://gitlab.com/rcmip/pyrcmip/merge_requests/19>`_) Clearer error message if the timeseries submission doesn't contain ``climate_model`` or ``unit`` metadata
- (`!17 <https://gitlab.com/rcmip/pyrcmip/merge_requests/17>`_) Update create-token script to allow for rotating of tokens

Changed
~~~~~~~

- (`!20 <https://gitlab.com/rcmip/pyrcmip/merge_requests/20>`_) Each input timeseries is now individually validated and uploaded when using the cli

v0.2.1 - 2020-09-01
-------------------

Added
~~~~~

- (`!18 <https://gitlab.com/rcmip/pyrcmip/merge_requests/18>`_) Clarification that pyrcmip only supports Python 3.6+
- (`!18 <https://gitlab.com/rcmip/pyrcmip/merge_requests/18>`_) Add support from submission from gzipped csv
- (`!16 <https://gitlab.com/rcmip/pyrcmip/merge_requests/16>`_) Add the ability to specify multiple timeseries files via the CLI. Closes (`#3 <https://gitlab.com/rcmip/pyrcmip/issues/3>`_)

v0.2.0 - 2020-08-17
-------------------

Added
~~~~~

- (`!14 <https://gitlab.com/rcmip/pyrcmip/merge_requests/14>`_) Check if the templates have changed during CI
- (`!12 <https://gitlab.com/rcmip/pyrcmip/merge_requests/12>`_) Add readthedocs configuration
- (`!10 <https://gitlab.com/rcmip/pyrcmip/merge_requests/10>`_) Documentation of submission process
- (`!6 <https://gitlab.com/rcmip/pyrcmip/merge_requests/6>`_) Skeleton of data processing, including illustrative model submission and processing pipeline
- (`!5 <https://gitlab.com/rcmip/pyrcmip/merge_requests/5>`_) Basic docs

Changed
~~~~~~~

- (`!13 <https://gitlab.com/rcmip/pyrcmip/merge_requests/13>`_) Fix broken documentation on readthedocs
- (`!8 <https://gitlab.com/rcmip/pyrcmip/merge_requests/8>`_) Upload data, metadata and model reported values together
- (`!7 <https://gitlab.com/rcmip/pyrcmip/merge_requests/7>`_) Require validation before uploading
- (`!6 <https://gitlab.com/rcmip/pyrcmip/merge_requests/6>`_) Submissions now require three parts: timeseries, model reported and metadata rather than only just one
- (`!4 <https://gitlab.com/rcmip/pyrcmip/merge_requests/4>`_) Require scmdata >= 0.6.1

v0.1.1 - 2020-07-09
-------------------

Changed
~~~~~~~

- Fixed readme

v0.1.0 - 2020-07-09
-------------------

Added
~~~~~

- CLI framework
- Basic checks
