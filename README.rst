pyrcmip
=======

.. sec-begin-index

pyrcmip is a tool for validating and uploading results to `RCMIP <http://rcmip.org>`_.
The Reduced Complexity Model Intercomparison Project (RCMIP) is a project to evaluate reduced-complexity (also known as simple) climate models and compare them against CMIP coupled models.

.. image:: https://mybinder.org/badge_logo.svg
 :target: https://mybinder.org/v2/gl/rcmip%2Fpyrcmip/master?filepath=notebooks%2Fexample-model-pipeline
.. sec-end-index

.. sec-begin-installation

Installation
------------

The easiest way to install pyrcmip is with `pip <https://pypi.org/project/pip/>`_.
At this stage pyrcmip only supports Python 3.6+.

::

  # if you're using a virtual environment, make sure you're in it
  pip install pyrcmip

.. sec-end-installation

Documentation
-------------

Documentation can be found at our `documentation pages <https://pyrcmip.readthedocs.io/en/latest/>`_
(we are thankful to `Read the Docs <https://readthedocs.org/>`_ for hosting us).


Contributing
------------

If you'd like to contribute, please make a pull request!
The pull request templates should ensure that you provide all the necessary information.

.. sec-begin-license

License
-------

pyrcmip is free software under a BSD 3-Clause License, see `LICENSE <https://gitlab.com/rcmip/pyrcmip/blob/master/LICENSE>`_.

.. sec-end-license

If you make use of pyrcmip or any of the RCMIP project, please cite the articles provided in ``citations.bib``.
