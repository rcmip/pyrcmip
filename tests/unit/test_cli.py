import re
from os.path import exists, join

import click
import pandas as pd
import pytest
from conftest import check_file_uploaded

from pyrcmip.cli import validate_version


def test_cli_empty(run_cli):
    result = run_cli([])
    assert result.exit_code == 0
    assert "rcmip" in result.output
    assert re.search(r"--help\s*Show this message and exit.", result.output)
    assert "Commands:" in result.output


def test_cli_help(run_cli):
    help_result = run_cli(["--help"])
    assert help_result.exit_code == 0
    assert re.search(r"--help\s*Show this message and exit.", help_result.output)
    assert "Commands:" in help_result.output


@pytest.mark.parametrize(
    "v,exp",
    (
        ("v2.0.0", False),
        ("1.2.3", True),
        ("2.0.0", True),
        ("2.0.0-pre.2+build.4", False),
        ("2.0.0+build.4", False),
        ("2.0.0-pre.2", False),
    ),
)
def test_versions(v, exp):
    if exp:
        assert validate_version(None, None, v)
    else:
        with pytest.raises(click.BadParameter):
            assert validate_version(None, None, v)


def test_upload_bad_version(run_upload):
    res = run_upload(version="v2")
    assert res.exit_code == 2
    assert "Invalid value for '--version': Cannot parse version string" in res.output


def test_upload(bucket, run_upload):
    conn, bucket_name = bucket
    org = "myOrg"
    model = "testModel"
    version = "1.2.3"

    res = run_upload(bucket=bucket_name, model=model, version=version)
    assert res.exit_code == 0
    assert "All files uploaded successfully" in res.output

    # check if the completed file is uploaded
    check_file_uploaded(conn, bucket_name, org, model, version, "data-000.csv.gz")
    check_file_uploaded(conn, bucket_name, org, model, version, "model_reported.csv")
    check_file_uploaded(conn, bucket_name, org, model, version, "metadata.csv")
    assert conn.Object(bucket_name, "myOrg/testModel/1.2.3-complete").get() is not None


def test_upload_multi(test_data_dir, run_upload, bucket):
    conn, bucket_name = bucket
    org = "myOrg"
    model = "testModel"
    version = "1.2.3"

    upload_files = [
        join(test_data_dir, "rcmip_model_output_test_nonssps.nc"),
        join(test_data_dir, "rcmip_model_output_test_ssps.nc"),
        join(test_data_dir, "rcmip_model_reported_metrics_test.csv"),
        join(test_data_dir, "rcmip_model_metadata_test.csv"),
    ]

    res = run_upload(
        data=upload_files, bucket=bucket_name, model=model, version=version
    )
    assert res.exit_code == 0
    assert "All files uploaded successfully" in res.output
    assert re.search(
        r"^.*Reading .*rcmip_model_output_test_ssps.nc", res.output, re.MULTILINE
    )
    assert re.search(
        r"^.*Reading .*rcmip_model_output_test_nonssps.nc", res.output, re.MULTILINE
    )

    # check if the completed file is uploaded
    check_file_uploaded(conn, bucket_name, org, model, version, "data-000.csv.gz")
    check_file_uploaded(conn, bucket_name, org, model, version, "data-001.csv.gz")
    check_file_uploaded(conn, bucket_name, org, model, version, "model_reported.csv")
    check_file_uploaded(conn, bucket_name, org, model, version, "metadata.csv")
    assert conn.Object(bucket_name, "myOrg/testModel/1.2.3-complete").get() is not None


def test_upload_same_version(run_upload, bucket):
    conn, bucket_name = bucket
    res = run_upload(bucket=bucket_name, model="testModel", version="1.2.3")
    assert res.exit_code == 0
    assert "All files uploaded successfully" in res.output

    # Upload with same version
    res = run_upload(bucket=bucket_name, model="testModel", version="1.2.3")
    assert res.exit_code == 1
    assert "Data for this version has already been uploaded." in res.output

    # Incremented version should work
    res = run_upload(bucket=bucket_name, model="testModel", version="1.2.4")
    assert res.exit_code == 0
    assert "All files uploaded successfully" in res.output

    # check if the completed file is uploaded
    assert conn.Object(bucket_name, "myOrg/testModel/1.2.3-complete").get() is not None
    assert conn.Object(bucket_name, "myOrg/testModel/1.2.4-complete").get() is not None


def test_upload_with_failed_validation(run_upload, mocker):
    mock_validation = mocker.patch("pyrcmip.cli.validate_submission_bundle")
    mock_validation.side_effect = ValueError("Validation failed")

    res = run_upload(model="testModel", version="1.2.3")

    assert res.exit_code == 1, res.output
    assert "Validation failed" in res.output


def test_download(run_upload, run_download, bucket):
    conn, bucket_name = bucket
    run_upload(bucket=bucket_name, model="testModel", version="1.2.3")

    run_download_func, output_dir = run_download

    res = run_download_func(bucket=bucket_name, model="testModel", version="1.2.3")

    assert res.exit_code == 0, res.output

    def can_read(fname):
        assert exists(join(output_dir, fname))
        run = pd.read_csv(join(output_dir, fname))
        assert len(run)

    can_read("rcmip-testModel-1.2.3-data-000.csv.gz")
    can_read("rcmip-testModel-1.2.3-metadata.csv")
    can_read("rcmip-testModel-1.2.3-model_reported.csv")


def test_download_missing(run_upload, run_download, bucket):
    conn, bucket_name = bucket
    run_download_func, output_dir = run_download

    res = run_download_func(bucket=bucket_name, model="testModel", version="1.2.4")

    assert res.exit_code == 1
    assert "No files for testModel==1.2.4 have been uploaded" in res.output
