import os.path
import re
from unittest.mock import call, patch

import numpy as np
import pandas as pd
import pytest
from scmdata import ScmRun

from pyrcmip.database import Database

MOCK_ROOT_DIR_NAME = os.path.join("/mock", "root", "dir")


@pytest.fixture()
def start_scmrun():
    return ScmRun(
        np.arange(6).reshape(3, 2),
        [2010, 2020, 2030],
        columns={
            "scenario": "scenario",
            "model": "model",
            "climate_model": ["cmodel_a", "cmodel_b"],
            "variable": "variable",
            "unit": "unit",
            "region": "region",
            "ensemble_member": 0,
        },
    )


@pytest.fixture()
def tdb():
    return Database(MOCK_ROOT_DIR_NAME)


def test_database_init_and_repr():
    tdb = Database("root_dir")
    assert tdb._root_dir == "root_dir"
    assert "root_dir: root_dir" in str(tdb)


@pytest.mark.parametrize(
    "inp,exp_tail",
    (
        (
            {
                "climate_model": "cm",
                "variable": "v",
                "region": "r",
                "scenario": "s",
                "ensemble_member": "em",
            },
            os.path.join("cm", "v", "r", "s", "cm_v_r_s_em.nc"),
        ),
        (
            {"climate_model": "cm", "variable": "v", "region": "r", "scenario": "s"},
            os.path.join("cm", "v", "r", "s", "cm_v_r_s.nc"),
        ),
        (
            {
                "climate_model": "MAGICC 7.1.0",
                "variable": "Emissions|CO2",
                "region": "World|R5.2OECD90",
                "scenario": "1pctCO2-bgc",
                "ensemble_member": "001",
            },
            os.path.join(
                "MAGICC-7.1.0",
                "Emissions-CO2",
                "World-R5.2OECD90",
                "1pctCO2-bgc",
                "MAGICC-7.1.0_Emissions-CO2_World-R5.2OECD90_1pctCO2-bgc_001.nc",
            ),
        ),
        (
            {
                "climate_model": "MAGICC7.1.0",
                "variable": "Emissions|CO2",
                "region": "World|R5.2OECD90",
                "scenario": "1pctCO2-bgc",
            },
            os.path.join(
                "MAGICC7.1.0",
                "Emissions-CO2",
                "World-R5.2OECD90",
                "1pctCO2-bgc",
                "MAGICC7.1.0_Emissions-CO2_World-R5.2OECD90_1pctCO2-bgc.nc",
            ),
        ),
    ),
)
def test_get_out_filepath(inp, exp_tail, tdb):
    res = tdb.get_out_filepath(**inp)
    exp = os.path.join(tdb._root_dir, exp_tail)

    assert res == exp


@patch("pyrcmip.database.ensure_dir_exists")
@patch.object(Database, "get_out_filepath")
@patch.object(ScmRun, "to_nc")
def test_save_to_database_single_file(
    mock_to_nc, mock_get_out_filepath, mock_ensure_dir_exists, tdb, start_scmrun
):
    tout_file = "test_out.nc"
    mock_get_out_filepath.return_value = tout_file
    inp_scmrun = start_scmrun.filter(climate_model="cmodel_a")

    tdb._save_to_database_single_file(inp_scmrun)

    mock_get_out_filepath.assert_called_once()
    mock_get_out_filepath.assert_called_with(
        inp_scmrun.get_unique_meta("climate_model", no_duplicates=True),
        inp_scmrun.get_unique_meta("variable", no_duplicates=True),
        inp_scmrun.get_unique_meta("region", no_duplicates=True),
        inp_scmrun.get_unique_meta("scenario", no_duplicates=True),
        inp_scmrun.get_unique_meta("ensemble_member", no_duplicates=True),
    )

    mock_ensure_dir_exists.assert_called_once()
    mock_ensure_dir_exists.assert_called_with(tout_file)

    mock_to_nc.assert_called_once()
    mock_to_nc.assert_called_with(tout_file)


def test_save_to_database_single_file_non_unique_meta(tdb, start_scmrun):
    with pytest.raises(
        ValueError,
        match=re.escape(
            "`climate_model` column is not unique (found values: ['cmodel_a', 'cmodel_b'])"
        ),
    ):
        tdb._save_to_database_single_file(start_scmrun)


def test_save_to_database_single_file_no_ensemble_member(tdb, start_scmrun):
    with pytest.raises(KeyError, match=re.escape("Level ensemble_member not found")):
        tdb._save_to_database_single_file(
            start_scmrun.filter(climate_model="cmodel_a").drop_meta(
                "ensemble_member", inplace=False
            )
        )


@patch.object(Database, "_save_to_database_single_file")
def test_save_to_database(mock_save_to_database_single_file, tdb, start_scmrun):
    tdb.save_to_database(start_scmrun)

    expected_calls = len(
        list(
            start_scmrun.groupby(
                ["climate_model", "variable", "region", "scenario", "ensemble_member"]
            )
        )
    )
    assert mock_save_to_database_single_file.call_count == expected_calls


@patch("pyrcmip.database.ensure_dir_exists")
@patch.object(ScmRun, "to_nc")
def test_save_condensed_file(mock_to_nc, mock_ensure_dir_exists, tdb, start_scmrun):
    to_save = start_scmrun.filter(climate_model="cmodel_a")
    tdb.save_condensed_file(to_save)

    expected_out_file = tdb.get_out_filepath(
        *[
            to_save.get_unique_meta(k, no_duplicates=True)
            for k in ["climate_model", "variable", "region", "scenario"]
        ]
    )
    mock_ensure_dir_exists.assert_called_once()
    mock_ensure_dir_exists.assert_called_with(expected_out_file)

    mock_to_nc.assert_called_once()
    mock_to_nc.assert_called_with(expected_out_file, dimensions=("ensemble_member",))


def test_save_condensed_file_no_ensemble_member_error(tdb, start_scmrun):
    error_msg = re.escape("`scmrun` must contain ensemble_member metadata")
    with pytest.raises(AssertionError, match=error_msg):
        tdb.save_condensed_file(
            start_scmrun.filter(climate_model="cmodel_a").drop_meta(
                "ensemble_member", inplace=False
            )
        )


@pytest.mark.parametrize(
    "diff_col", ("climate_model", "variable", "region", "scenario")
)
def test_save_condensed_file_non_unique_meta_error(tdb, start_scmrun, diff_col):
    start_scmrun["run_id"] = range(start_scmrun.shape[0])
    start_scmrun["climate_model"] = "cmodel_a"

    start_scmrun[diff_col] = [str(v) for v in range(start_scmrun.shape[0])]

    with pytest.raises(ValueError, match="`{}` column is not unique".format(diff_col)):
        tdb.save_condensed_file(start_scmrun)


@patch("pyrcmip.database.glob")
@patch("pyrcmip.database.run_append")
@patch.object(ScmRun, "from_nc")
def test_load_data(mock_from_nc, mock_run_append, mock_glob, tdb, start_scmrun):
    tclimate_model = "cm"
    tvariable = "v"
    tregion = "r"
    tscenario = "s"

    tfiles = ["tfile1", "tfile2"]
    mock_glob.glob.return_value = tfiles

    tscmrun = "mock return value"
    mock_from_nc.return_value = tscmrun

    tdb.load_data(tclimate_model, tvariable, tregion, tscenario)

    exp_load_path = os.path.join(
        tdb._root_dir, tclimate_model, tvariable, tregion, tscenario
    )
    exp_glob = tdb._get_disk_filename(os.path.join(exp_load_path, "**", "*.nc"))

    mock_glob.glob.assert_called_with(exp_glob, recursive=True)

    exp_nc_calls = [call(f) for f in tfiles]
    mock_from_nc.assert_has_calls(exp_nc_calls)

    mock_run_append.assert_called_with([tscmrun] * len(tfiles))


@patch("pyrcmip.database.ensure_dir_exists")
@pytest.mark.parametrize("key", (None, "tkey"))
def test_save_model_reported(mock_ensure_dir_exists, key, tdb, valid_model_reported):
    if key is None:
        _key = "all"
    else:
        _key = key

    with patch.object(valid_model_reported, "to_csv") as mock_to_csv:
        if key is not None:
            tdb.save_model_reported(valid_model_reported, key=key)
        else:
            tdb.save_model_reported(valid_model_reported)

    expected_out_file = tdb._get_disk_filename(
        os.path.join(
            tdb._root_dir,
            valid_model_reported["climate_model"].unique()[0],
            "model_reported_metrics_{}.csv".format(_key),
        )
    )

    mock_ensure_dir_exists.assert_called_with(expected_out_file)
    mock_to_csv.assert_called_with(expected_out_file, index=False)


def test_save_model_reported_extra_col(tdb, valid_model_reported):
    valid_model_reported["junk"] = "junk"

    expected_columns = {
        "value",
        "ensemble_member",
        "RCMIP name",
        "unit",
        "climate_model",
    }
    error_msg = re.escape(
        "Input columns: {}. Expected columns: {}.".format(
            set(valid_model_reported.columns), expected_columns
        )
    )
    with pytest.raises(AssertionError, match=error_msg):
        tdb.save_model_reported(valid_model_reported)


def test_save_model_reported_multi_climate_model(tdb, valid_model_reported):
    valid_model_reported.loc[0, "climate_model"] = (
        valid_model_reported.loc[0, "climate_model"] + "a"
    )

    error_msg = re.escape(
        "More than one climate model: {}".format(
            valid_model_reported["climate_model"].unique().tolist()
        )
    )
    with pytest.raises(AssertionError, match=error_msg):
        tdb.save_model_reported(valid_model_reported)


@patch("pyrcmip.database.pd")
@patch("pyrcmip.database.glob")
def test_load_model_reported(mock_glob, mock_pd, tdb):
    tfiles = ["tfile1", "tfile2"]

    mock_glob.glob.return_value = tfiles

    rcsv = "mocked read csv"
    mock_pd.read_csv.return_value = rcsv

    rv = 13.4
    mock_pd.concat.return_value = rv

    res = tdb.load_model_reported()

    assert res == rv

    exp_glob_path = tdb._get_disk_filename(
        os.path.join(tdb._root_dir, "**", "model_reported_metrics*.csv")
    )
    mock_glob.glob.assert_called_with(exp_glob_path, recursive=True)

    mock_pd.read_csv.assert_has_calls([call(f) for f in tfiles])
    mock_pd.concat.assert_called_with([rcsv] * len(tfiles))


@pytest.fixture()
def valid_summary():
    return pd.DataFrame(
        [
            {
                "assessed_range_label": "central",
                "assessed_range_value": 1.5,
                "climate_model": "two_layer",
                "climate_model_value": 1.5,
                "RCMIP name": "Equilibrium Climate Sensitivity",
                "percentage_difference": 0,
                "unit": "K",
            },
            {
                "assessed_range_label": "likely_lower",
                "assessed_range_value": 1.4,
                "climate_model": "two_layer",
                "climate_model_value": 1.45,
                "RCMIP name": "Equilibrium Climate Sensitivity",
                "percentage_difference": (1.45 - 1.4) / 1.4,
                "unit": "K",
            },
            {
                "assessed_range_label": "central",
                "assessed_range_value": 1.5,
                "climate_model": "MAGICC7",
                "climate_model_value": 1.6,
                "RCMIP name": "Equilibrium Climate Sensitivity",
                "percentage_difference": 0.1 / 1.5,
                "unit": "K",
            },
        ]
    )


@patch("pyrcmip.database.ensure_dir_exists")
@pytest.mark.parametrize("file_id", ("erf", "tkey"))
def test_save_summary_table(mock_ensure_dir_exists, file_id, tdb, valid_summary):
    with patch.object(valid_summary, "to_csv") as mock_to_csv:
        tdb.save_summary_table(valid_summary, file_id)

    expected_out_file = tdb._get_disk_filename(
        os.path.join(
            tdb._root_dir,
            "climate_model_assessed_ranges_summary_table_{}.csv".format(file_id),
        )
    )

    mock_ensure_dir_exists.assert_called_with(expected_out_file)
    mock_to_csv.assert_called_with(expected_out_file, index=False)


def test_save_summary_table_extra_col(tdb, valid_summary):
    valid_summary["junk"] = "junk"

    expected_columns = {
        "assessed_range_label",
        "assessed_range_value",
        "climate_model",
        "climate_model_value",
        "RCMIP name",
        "percentage_difference",
        "unit",
    }
    error_msg = re.escape(
        "Input columns: {}. Expected columns: {}.".format(
            set(valid_summary.columns), expected_columns
        )
    )
    with pytest.raises(AssertionError, match=error_msg):
        tdb.save_summary_table(valid_summary, "file_id")


@patch("pyrcmip.database.pd")
@patch("pyrcmip.database.glob")
def test_load_summary_tables(mock_glob, mock_pd, tdb):
    tfiles = ["tfile1", "tfile2"]

    mock_glob.glob.return_value = tfiles

    rcsv = "mocked read csv"
    mock_pd.read_csv.return_value = rcsv

    rv = 13.4
    mock_pd.concat.return_value = rv

    res = tdb.load_summary_tables()

    assert res == rv

    exp_glob_path = tdb._get_disk_filename(
        os.path.join(tdb._root_dir, "climate_model_assessed_ranges_summary_table_*.csv")
    )
    mock_glob.glob.assert_called_with(exp_glob_path)

    mock_pd.read_csv.assert_has_calls([call(f) for f in tfiles])
    mock_pd.concat.assert_called_with([rcsv] * len(tfiles))
