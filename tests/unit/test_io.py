import gzip
import io
from unittest.mock import Mock

import boto3
import botocore.exceptions
import click
import pandas as pd
import pytest
from conftest import check_file_uploaded, get_upload_key
from scmdata import ScmRun
from scmdata.testing import assert_scmdf_almost_equal

from pyrcmip.io import _upload_file, temporary_file_to_upload


def can_read_gzipped(base_run, stream):
    stream.seek(0)
    with gzip.GzipFile(fileobj=stream, mode="r") as gzip_fh:
        run = ScmRun(pd.read_csv(gzip_fh))

    assert_scmdf_almost_equal(base_run, run, check_ts_names=False)


def test_temp_file(phase_1_results):
    out_fh = io.BytesIO()
    with temporary_file_to_upload(phase_1_results, max_size=5, compress=True) as fh:
        out_fh.write(fh.read())

        # Check that the file is still in memory
        assert out_fh.tell() < 5 * 1024 * 1024  # 5MB
        assert isinstance(fh._file, io.BytesIO)

        can_read_gzipped(phase_1_results, fh)
        can_read_gzipped(phase_1_results, out_fh)

    out_fh = io.BytesIO()
    with temporary_file_to_upload(phase_1_results, max_size=0.5, compress=True) as fh:
        out_fh.write(fh.read())

        # Check that the file has been buffered to disk
        assert out_fh.tell() > 0.5 * 1024 * 1024  # 0.5MB
        assert isinstance(fh._file, io.BufferedRandom)

        can_read_gzipped(phase_1_results, fh)
        can_read_gzipped(phase_1_results, out_fh)


@pytest.mark.parametrize("compress", [True, False])
def test_upload_file(phase_1_results, bucket, test_data_dir, compress):
    conn, bucket_name = bucket
    client = boto3.client("s3")

    org = "test"
    model = "model"
    version = "v0.1.1"

    _upload_file(
        client,
        bucket_name,
        get_upload_key(org, model, version, "test.csv"),
        phase_1_results,
        compress=compress,
    )

    if compress:
        check_file_uploaded(conn, bucket_name, org, model, version, "test.csv.gz")
    else:
        check_file_uploaded(conn, bucket_name, org, model, version, "test.csv")


def test_upload_file_failed(phase_1_results, bucket, test_data_dir):
    conn, bucket_name = bucket
    client = boto3.client("s3")
    client.upload_fileobj = Mock(side_effect=botocore.exceptions.ClientError({}, "s3"))

    org = "test"
    model = "model"
    version = "v0.1.1"

    with pytest.raises(click.ClickException, match="Failed to upload file"):
        _upload_file(
            client,
            bucket_name,
            get_upload_key(org, model, version, "test.csv"),
            phase_1_results,
        )


def test_upload_file_failed_other(bucket, test_data_dir, phase_1_results):
    client = boto3.client("s3")
    client.upload_fileobj = Mock(side_effect=ValueError("Something went wrong"))

    org = "test"
    model = "model"
    version = "v0.1.1"

    with pytest.raises(Exception, match="Something went wrong"):
        _upload_file(
            client,
            bucket[1],
            get_upload_key(org, model, version, "test.csv"),
            phase_1_results,
        )
