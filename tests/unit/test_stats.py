import re

import numpy as np
import numpy.testing as npt
import pandas as pd
import pytest

from pyrcmip.stats import (
    _get_skewed_normal_width,
    get_skewed_normal,
    sample_multivariate_skewed_normal,
)


# calculated using Nicolai's R script
@pytest.mark.parametrize(
    "ratio,conflevel,exp",
    (
        (1, 0.66, 1e-7),  # hits bounds
        (1, 0.9, 1e-7),  # hits bounds
        (1, 0.95, 1e-7),  # hits bounds
        (0.98, 0.9, 0.01228),
        (0.98, 0.66, 0.02118342),
        (1.666667, 0.66, 0.5353549),
        (1.666667, 0.9, 0.3105594),
        (0.5, 0.66, 0.7264466),
        (0.5, 0.9, 0.4214226),
    ),
)
def test_get_skewed_normal_width(ratio, conflevel, exp):
    res = _get_skewed_normal_width(ratio, conflevel)

    npt.assert_allclose(res, exp, rtol=1e-3)


@pytest.mark.parametrize(
    "median,lower,upper,conf,exp,rtol",
    (
        (
            0.5,
            1,
            0.01,
            0.66,
            [1.1738983, 0.7735442, 0.5000000, 0.2294777, -0.1558497],
            1e-4,
        ),
        (
            0.5,
            0.01,
            1,
            0.66,
            [1.1738983, 0.7735442, 0.5000000, 0.2294777, -0.1558497][::-1],
            1e-4,
        ),
        (4, 3.6, 4.5, 0.9, [3.680837, 3.862662, 4.000000, 4.147465, 4.379771], 1e-6),
        (4, 3.6, 4.5, 0.66, [3.482082, 3.769172, 4.000000, 4.260948, 4.698929], 1e-5),
        (
            -1,
            -2,
            -0.5,
            0.66,
            [-2.5369821, -1.4636702, -1.0000000, -0.6832146, -0.3941712],
            1e-4,
        ),
        (
            -1,
            -2,
            -0.5,
            0.9,
            [-1.7160904, -1.2473007, -1.0000000, -0.8017337, -0.5827308],
            1e-4,
        ),
        (
            -1,
            -2,
            -0.5,
            0.95,
            [-1.5733812, -1.2037697, -1.0000000, -0.8307219, -0.6355655],
            1e-4,
        ),
    ),
)
def test_get_skewed_normal(median, lower, upper, conf, exp, rtol):
    input_data = [0.1, 0.3, 0.5, 0.7, 0.9]

    res = get_skewed_normal(median, lower, upper, conf, input_data)

    npt.assert_allclose(res, exp, rtol=rtol)


@pytest.mark.parametrize(
    "median,lower,upper,conf",
    (
        # (0.5, 1, 0.01, 0.66,),  # need to actually return a normal to make these symmetric cases work
        # (0.5, 0.01, 1, 0.66,),
        (4, 3.6, 4.5, 0.9,),
        (4, 3.6, 4.5, 0.66,),
        (-1, -2, -0.5, 0.66,),
        (-1, -2, -0.5, 0.9,),
        (-1, -2, -0.5, 0.95,),
    ),
)
def test_get_skewed_normal_levels(median, lower, upper, conf):
    input_data = np.random.random(10 ** 6)

    res = get_skewed_normal(median, lower, upper, conf, input_data)

    # thanks https://stackoverflow.com/a/24788741
    res_sorted = np.sort(res)
    if upper < lower:
        res_sorted = res_sorted[::-1]

    p = 1.0 * np.arange(len(res)) / (len(res) - 1)

    lower_level = (1 - conf) / 2
    upper_level = 1 - lower_level

    npt.assert_allclose(lower, res_sorted[np.argmax(p > lower_level)], rtol=2e-2)
    npt.assert_allclose(upper, res_sorted[np.argmax(p > upper_level)], rtol=2e-2)


def _check_marginals(res, inp, rtol, atol):
    for label, col in res.items():
        npt.assert_allclose(
            col.median(), inp.loc["median", label], rtol=rtol, atol=atol,
        )

        lower_level = 0.5 - inp.loc["conf", label] / 2
        npt.assert_allclose(
            col.quantile(q=lower_level), inp.loc["lower", label], rtol=rtol, atol=atol,
        )

        upper_level = 1 - lower_level
        npt.assert_allclose(
            col.quantile(q=upper_level), inp.loc["upper", label], rtol=rtol, atol=atol,
        )


@pytest.mark.parametrize(
    "inp",
    (
        pd.DataFrame(
            [{"median": 0, "upper": 1, "lower": -2, "conf": 0.90}], index=["para"],
        ).T,
        pd.DataFrame(
            [{"median": -1, "upper": -0.5, "lower": -2, "conf": 0.66}], index=["para"],
        ).T,
    ),
)
# results along marginal axes should be independent of correlation matrix
@pytest.mark.parametrize("correlation", (None, [[1]]))
@pytest.mark.parametrize("size", (10 ** 6, int(1.3 * 10 ** 6)))
def test_sample_multivariate_skewed_normal_1d(inp, correlation, size):
    rtol = 1.5 * 1e-2
    atol = 1e-2
    res = sample_multivariate_skewed_normal(inp, size, cor=correlation)

    assert res.columns.equals(inp.columns)
    assert res.shape[0] == size

    _check_marginals(res, inp, rtol, atol)


@pytest.mark.parametrize(
    "inp",
    (
        pd.DataFrame(
            [
                {"median": 0, "upper": 1, "lower": -2, "conf": 0.90},
                {"median": 10, "upper": 12, "lower": 3, "conf": 0.66},
            ],
            index=["para", "other_para"],
        ).T,
        pd.DataFrame(
            [
                {"median": 1, "upper": 2, "lower": -2, "conf": 0.90},
                {"median": 0.1, "upper": 0.3, "lower": -0.1, "conf": 0.95},
            ],
            index=["para", "other_para"],
        ).T,
    ),
)
# results along marginal axes should be independent of correlation matrix
@pytest.mark.parametrize("correlation", (None, [[1, 0], [0, 1]], [[1, 0.9], [0.9, 1]],))
@pytest.mark.parametrize("size", (10 ** 6, int(1.3 * 10 ** 6)))
def test_sample_multivariate_skewed_normal(inp, correlation, size):
    rtol = 1.5 * 1e-2
    atol = 1e-2
    res = sample_multivariate_skewed_normal(inp, size, cor=correlation)

    assert res.columns.equals(inp.columns)
    assert res.shape[0] == size

    _check_marginals(res, inp, rtol, atol)


@pytest.mark.parametrize(
    "correlation_inp",
    (
        None,
        [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
        [[1, 0.9, 0], [0.9, 1, 0], [0, 0, 1]],
        [[1, -0.9, 0], [-0.9, 1, 0], [0, 0, 1]],
        [[1, 0, -0.9], [0, 1, 0], [-0.9, 0, 1]],
        [[1, 0.9, -0.3], [0.9, 1, -0.2], [-0.3, -0.2, 1]],
    ),
)
def test_sample_multivariate_skewed_normal_correlation(correlation_inp):
    if correlation_inp is None:
        correlation_exp = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    else:
        correlation_exp = correlation_inp

    size = 5 * 10 ** 5
    inp = pd.DataFrame(
        [
            {"median": 0, "upper": 1, "lower": -2, "conf": 0.90},
            {"median": 10, "upper": 12, "lower": 3, "conf": 0.66},
            {"median": -2, "upper": -0.5, "lower": -3, "conf": 0.95},
        ],
        index=["para", "other_para", "final_para"],
    ).T

    rtol = 5 * 1e-2
    atol = 3 * 1e-2

    res = sample_multivariate_skewed_normal(inp, size, cor=correlation_inp)
    _check_marginals(res, inp, rtol=1e-2, atol=0.1)

    # use spearman correlation coefficient because the stretching in different
    # axes mucks with standard Pearson measures
    res_correlations = res.corr(method="spearman")
    npt.assert_allclose(correlation_exp, res_correlations, atol=atol, rtol=rtol)


def test_sample_multivariate_skewed_normal_not_normalised_correlation_diagonal():
    inp = pd.DataFrame(
        [
            {"median": 0, "upper": 1, "lower": -2, "conf": 0.90},
            {"median": 10, "upper": 12, "lower": 3, "conf": 0.66},
        ],
        index=["para", "other_para"],
    ).T

    cor = [[1.1, 0], [0, 1.1]]
    error_msg = re.escape(
        "The correlation matrix must be normalised i.e. all "
        "diagonal elements should be equal to 1. "
        f"Received: {np.asarray(cor)}"
    )
    with pytest.raises(ValueError, match=error_msg):
        sample_multivariate_skewed_normal(inp, size=2, cor=cor)


def test_sample_multivariate_skewed_normal_not_normalised_correlation_off_diagnoal():
    inp = pd.DataFrame(
        [
            {"median": 0, "upper": 1, "lower": -2, "conf": 0.90},
            {"median": 10, "upper": 12, "lower": 3, "conf": 0.66},
        ],
        index=["para", "other_para"],
    ).T

    cor = [[1, -10], [-10, 1]]
    error_msg = re.escape(
        "The correlation matrix must be normalised i.e. all "
        "off-diagonal elements should have magnitude <=1. "
        f"Received: {np.asarray(cor)}"
    )
    with pytest.raises(ValueError, match=error_msg):
        sample_multivariate_skewed_normal(inp, size=2, cor=cor)


def test_sample_multivariate_skewed_normal_out_of_order_median_gt_upper():
    inp = pd.DataFrame(
        [
            {"median": 0, "upper": -1, "lower": -2, "conf": 0.90},
            {"median": 10, "upper": 12, "lower": 3, "conf": 0.66},
        ],
        index=["para", "other_para"],
    ).T

    medians = np.asarray(inp.loc["median", :])
    uppers = np.asarray(inp.loc["upper", :])

    error_msg = re.escape(
        f"uppers must be greater than medians. Received uppers: {uppers} "
        f"and medians: {medians}. Entries where uppers are greater than "
        f"medians: {uppers > medians}"
    )
    with pytest.raises(ValueError, match=error_msg):
        sample_multivariate_skewed_normal(inp, size=2)


def test_sample_multivariate_skewed_normal_out_of_order_lower_gt_median():
    inp = pd.DataFrame(
        [
            {"median": -1.5, "upper": -1, "lower": -2, "conf": 0.90},
            {"median": 10, "upper": 12, "lower": 11, "conf": 0.66},
        ],
        index=["para", "other_para"],
    ).T

    medians = np.asarray(inp.loc["median", :])
    lowers = np.asarray(inp.loc["lower", :])

    error_msg = re.escape(
        f"medians must be greater than lowers. Received medians: {medians} "
        f"and lowers: {lowers}. Entries where medians are greater than "
        f"lowers: {medians > lowers}"
    )
    with pytest.raises(ValueError, match=error_msg):
        sample_multivariate_skewed_normal(inp, size=2)


@pytest.mark.parametrize("missing_col", ("median", "lower", "upper", "conf"))
def test_sample_multivariate_skewed_normal_input_missing_required_row(missing_col):
    inp = pd.DataFrame(
        [{"median": -1.5, "upper": -1, "lower": -2, "conf": 0.9}], index=["para"],
    ).T.drop(missing_col, axis="rows")

    with pytest.raises(KeyError, match=missing_col):
        sample_multivariate_skewed_normal(inp, size=2)


def test_sample_multivariate_skewed_normal_input_contains_nans():
    inp = pd.DataFrame(
        [
            {"median": -1.5, "upper": -1, "lower": -2, "conf": 0.9},
            {"median": 1, "upper": 2.2, "lower": 0.2},
        ],
        index=["para", "other_para"],
    ).T

    error_msg = re.escape(f"`configuration` must not contain nan, received: {inp}")
    with pytest.raises(ValueError, match=error_msg):
        sample_multivariate_skewed_normal(inp, size=2)


def test_confidence_interval_outside_range():
    error_msg = re.escape("Confidence level must be in [0, 1]. Received: 10.0")
    with pytest.raises(ValueError, match=error_msg):
        _get_skewed_normal_width(2, 10.0)
