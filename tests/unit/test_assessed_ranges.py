import re
from unittest.mock import patch

import numpy as np
import numpy.testing as npt
import pandas as pd
import pandas.testing as pdt
import pytest

from pyrcmip.assessed_ranges import AssessedRanges


def test_db(assessed_ranges_sample_df, assessed_ranges_sample):
    pdt.assert_frame_equal(assessed_ranges_sample_df, assessed_ranges_sample.db)


def test_init_non_unique_metric_raises(assessed_ranges_sample_df):
    assessed_ranges_sample_df.loc[1, "RCMIP name"] = assessed_ranges_sample_df.loc[
        0, "RCMIP name"
    ]

    error_msg = re.escape(
        "Your assessed ranges do not have unique values for `RCMIP name`. Duplicates: {}".format(
            [assessed_ranges_sample_df.loc[0, "RCMIP name"]]
        )
    )
    with pytest.raises(ValueError, match=error_msg):
        AssessedRanges(assessed_ranges_sample_df)


@pytest.mark.parametrize("n", (1, 3, 7))
def test_head(assessed_ranges_sample, n):
    pdt.assert_frame_equal(
        assessed_ranges_sample.head(n), assessed_ranges_sample.db.head(n)
    )


@pytest.mark.parametrize("n", (1, 3, 7))
def test_tail(assessed_ranges_sample, n):
    pdt.assert_frame_equal(
        assessed_ranges_sample.tail(n), assessed_ranges_sample.db.tail(n)
    )


@pytest.mark.parametrize(
    "metric,col,expected",
    (
        ("Equilibrium Climate Sensitivity", "very_likely__lower", 2.3),
        ("Equilibrium Climate Sensitivity", "RCMIP variable", np.nan),
        (
            "Cumulative Net Land to Atmosphere Flux|CO2 World esm-hist-2011",
            "evaluation_period_end",
            2011,
        ),
    ),
)
def test_get_col_for_metric(assessed_ranges_sample, metric, col, expected):
    res = assessed_ranges_sample.get_col_for_metric(metric, col)

    if np.isnan(expected):
        assert np.isnan(res)
    else:
        assert res == expected


def test_get_col_for_metric_bad_metric_error(assessed_ranges_sample):
    error_msg = re.escape("Metric `{}` not found".format("junk"))
    with pytest.raises(ValueError, match=error_msg):
        assessed_ranges_sample.get_col_for_metric("junk", "very_likely__lower")


def test_get_col_for_metric_bad_col_error(assessed_ranges_sample):
    error_msg = re.escape("Column `{}` not found".format("junk"))
    with pytest.raises(KeyError, match=error_msg):
        assessed_ranges_sample.get_col_for_metric(
            "Equilibrium Climate Sensitivity", "junk"
        )


@pytest.mark.parametrize(
    "metric,col,expected",
    (
        (
            "Heat Content|Ocean World ssp245 1971-2018",
            "RCMIP variable",
            ["Heat Content|Ocean"],
        ),
        (
            "Transient Climate Response to Emissions",
            "RCMIP variable",
            ["Surface Air Temperature Change", "Cumulative Emissions|CO2"],
        ),
    ),
)
def test_get_col_for_metric_list(assessed_ranges_sample, metric, col, expected):
    res = assessed_ranges_sample.get_col_for_metric_list(metric, col)

    assert res == expected


@pytest.mark.parametrize(
    "metric,col,val",
    (
        ("Equilibrium Climate Sensitivity", "RCMIP variable", np.nan),
        ("Equilibrium Climate Sensitivity", "very_likely__lower", 2.3),
    ),
)
def test_get_col_for_metric_list_type_error(assessed_ranges_sample, metric, col, val):
    error_msg = re.escape("Cannot split `{}` of type `{}`".format(val, type(val)))
    with pytest.raises(TypeError, match=error_msg):
        assessed_ranges_sample.get_col_for_metric_list(metric, col)


@pytest.mark.parametrize(
    "metric,expected",
    (
        (
            "Heat Content|Ocean World ssp245 1971-2018",
            {
                "variable": ["Heat Content|Ocean"],
                "region": ["World"],
                "scenario": ["ssp245"],
            },
        ),
        (
            "Transient Climate Response to Emissions",
            {
                "variable": [
                    "Surface Air Temperature Change",
                    "Cumulative Emissions|CO2",
                ],
                "region": ["World"],
                "scenario": ["1pctCO2"],
            },
        ),
    ),
)
def test_get_variables_regions_scenarios_for_metric(
    assessed_ranges_sample, metric, expected
):
    res = assessed_ranges_sample.get_variables_regions_scenarios_for_metric(metric)

    assert res == expected


@pytest.mark.parametrize(
    "metric,expected",
    (
        (
            "Surface Air Ocean Blended Temperature Change World ssp245 2000-2019",
            (range(1961, 1991), range(2000, 2020)),
        ),
        ("Transient Climate Response", (None, None)),
        (
            "Effective Radiative Forcing|Anthropogenic|CH4 World historical-1750",
            (range(1750, 1751), range(2011, 2012)),
        ),
        (
            "Net Ocean to Atmosphere Flux|CO2 World esm-hist-1980",
            (None, range(1980, 1990)),
        ),
    ),
)
def test_get_norm_period_evaluation_period(assessed_ranges_sample, metric, expected):
    res = assessed_ranges_sample.get_norm_period_evaluation_period(metric)

    assert res == expected


@pytest.mark.parametrize(
    "col_to_break",
    (
        "evaluation_period_start",
        "evaluation_period_end",
        "norm_period_start",
        "norm_period_end",
    ),
)
def test_get_norm_period_evaluation_period_mismatch_raises(
    assessed_ranges_sample_df, col_to_break
):
    metric = "Transient Climate Response"
    assessed_ranges_sample_df.loc[
        assessed_ranges_sample_df["RCMIP name"] == metric, col_to_break
    ] = 1920.0

    ars = AssessedRanges(assessed_ranges_sample_df)

    error_msg = "^Ambiguous.*period.*$"
    with pytest.raises(ValueError, match=error_msg):
        ars.get_norm_period_evaluation_period(metric)


@pytest.mark.parametrize("n_to_draw", (None, 10, 10 ** 3))
@pytest.mark.parametrize(
    "metric,exp_kwargs",
    (
        (
            "Equilibrium Climate Sensitivity",
            {
                "median": 3.1,
                "lower": 2.3,
                "upper": 4.7,
                "conf": 0.9,  # very likely preferenced over likely
                "input_data": "mocked input_data value",  # thanks to mocking
            },
        ),
        (
            "Heat Content|Ocean World ssp245 1971-2018",
            {
                "median": 320.69251537323,
                "lower": 303.67217146117883,
                "upper": 337.7128592852812,
                "conf": 0.66,
                "input_data": "mocked input_data value",  # thanks to mocking
            },
        ),
    ),
)
@patch("pyrcmip.assessed_ranges.get_skewed_normal")
@patch("pyrcmip.assessed_ranges.np.random.random")
def test_get_assessed_range_for_boxplot(
    mock_np_random,
    mock_get_skewed_normal,
    n_to_draw,
    metric,
    exp_kwargs,
    assessed_ranges_sample,
):
    mock_np_random.return_value = "mocked input_data value"

    call_kwargs = {"metric": metric}
    if n_to_draw is None:
        _n_to_draw = 2 * 10 ** 4
    else:
        _n_to_draw = n_to_draw
        call_kwargs["n_to_draw"] = n_to_draw

    mock_skewed_normal_res = np.array([1, 4, 5, 13, -1, 4])
    mock_get_skewed_normal.return_value = mock_skewed_normal_res

    res = assessed_ranges_sample.get_assessed_range_for_boxplot(**call_kwargs)

    assert isinstance(res, pd.DataFrame)
    assert (res["Source"] == assessed_ranges_sample.assessed_range_label).all()
    assert (
        res["unit"] == assessed_ranges_sample.get_col_for_metric(metric, "unit")
    ).all()
    npt.assert_array_equal(res[metric].values, mock_skewed_normal_res)

    mock_np_random.assert_called_with(_n_to_draw)
    mock_get_skewed_normal.assert_called_with(**exp_kwargs)


def test_get_assessed_range_for_boxplot_central_nan(assessed_ranges_sample_df):
    metric = "Transient Climate Response"
    assessed_ranges_sample_df.loc[
        assessed_ranges_sample_df["RCMIP name"] == metric, "central"
    ] = np.nan

    ars = AssessedRanges(assessed_ranges_sample_df)
    res = ars.get_assessed_range_for_boxplot(metric)

    assert np.isnan(res[metric]).all()
