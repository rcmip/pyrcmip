import re
from unittest.mock import call, patch

import numpy as np
import pandas as pd
import pandas.testing as pdt
import pytest
from scmdata import ScmRun

from pyrcmip.errors import ProtocolConsistencyError
from pyrcmip.validate import (
    convert_units_to_rcmip_units,
    validate_regions,
    validate_scenarios,
    validate_submission,
    validate_submission_bundle,
    validate_submission_model_meta,
    validate_submission_model_reported_metrics,
    validate_variables,
)
from pyrcmip.validate.utils import load_submission_template_definitions


@pytest.mark.parametrize("protocol", ("a", "b"))
@patch("pyrcmip.validate.validate_submission_model_meta")
@patch("pyrcmip.validate.validate_submission_model_reported_metrics")
@patch("pyrcmip.validate.validate_submission")
def test_validate_submission_bundle(
    mock_validate_submission,
    mock_validate_submission_model_reported_metrics,
    mock_validate_submission_model_meta,
    protocol,
):
    test_model = "test_model v1.2.3"
    start = ScmRun(
        data=np.arange(6).reshape(3, 2),
        index=range(2000, 2021, 10),
        columns={
            "scenario": ["ssp119", "historical"],
            "model": "unspecified",
            "variable": "Surface Air Temperature Change",
            "region": "World",
            "unit": "K",
            "climate_model": test_model,
        },
    )
    mock_validate_submission.return_value = start

    meta = pd.DataFrame([{"test": "values with mocking", "climate_model": test_model}])
    mock_validate_submission_model_meta.return_value = meta

    model_reported = pd.DataFrame(
        [{"test": "model reported values with mocking", "climate_model": test_model}]
    )
    mock_validate_submission_model_reported_metrics.return_value = model_reported

    res = validate_submission_bundle(start, model_reported, meta, protocol)
    pdt.assert_frame_equal(
        res[0].timeseries().sort_index(),
        start.timeseries(res[0].meta.columns).sort_index(),
    )
    pdt.assert_frame_equal(res[1], model_reported)
    pdt.assert_frame_equal(res[2], meta)

    mock_validate_submission.assert_called_once()
    mock_validate_submission.assert_called_with(start, protocol=protocol)
    mock_validate_submission_model_meta.assert_called_once()
    mock_validate_submission_model_reported_metrics.assert_called_once()


@pytest.mark.parametrize("protocol", ("a", "b"))
@patch("pyrcmip.validate.validate_variables")
@patch("pyrcmip.validate.validate_regions")
@patch("pyrcmip.validate.validate_scenarios")
@patch("pyrcmip.validate.convert_units_to_rcmip_units")
@patch("pyrcmip.validate.load_submission_template_definitions")
def test_validate_submission_protocol_used(
    mock_load_submission_template_definitions,
    mock_convert_units_to_rcmip_units,
    mock_validate_scenarios,
    mock_validate_regions,
    mock_validate_variables,
    protocol,
):
    start = ScmRun(
        data=np.arange(6).reshape(3, 2),
        index=range(2000, 2021, 10),
        columns={
            "scenario": ["ssp119", "historical"],
            "model": "unspecified",
            "variable": "Surface Air Temperature Change",
            "region": "World",
            "unit": "K",
            "climate_model": "test_model v1.2.3",
            "ensemble_member": 1,
        },
    )
    mock_convert_units_to_rcmip_units.return_value = start

    res = validate_submission(start, protocol)
    pdt.assert_frame_equal(
        res.timeseries().sort_index(), start.timeseries(res.meta.columns).sort_index()
    )

    mock_validate_variables.assert_called_once()
    mock_validate_regions.assert_called_once()
    mock_validate_scenarios.assert_called_once()
    mock_convert_units_to_rcmip_units.assert_called_once()

    assert mock_load_submission_template_definitions.call_count == 3
    load_submission_template_definitions_calls = [
        call(protocol, component) for component in ["variables", "regions", "scenarios"]
    ]
    mock_load_submission_template_definitions.assert_has_calls(
        load_submission_template_definitions_calls, any_order=True
    )


@pytest.mark.parametrize(
    "vars_to_check",
    (
        [
            "Atmospheric Concentrations|CH4",
            "Atmospheric Concentrations|CO2",
            "Radiative Forcing|Anthropogenic|Aerosols|Aerosols-radiation Interactions|Biomass Burning|NH3",
            "Ocean pH",
            "Radiative Forcing|Natural|Volcanic",
        ],
        [
            "Atmospheric Concentrations|CH4",
            "Atmospheric Concentrations|CO2",
            "Ocean pH",
            "Radiative Forcing|Natural|Volcanic",
        ],
        [
            "Atmospheric Concentrations|CH4",
            "Atmospheric Concentrations|CO2",
            "Radiative Forcing|Anthropogenic|Aerosols|Aerosols-radiation Interactions|Biomass Burning|NH3",
        ],
        None,
    ),
)
def test_check_variables(vars_to_check, submission_template_xlsx):
    protocol_variables = load_submission_template_definitions(
        submission_template_xlsx, "variables"
    )["variable"].tolist()
    if vars_to_check is None:
        vars_to_check = protocol_variables

    validate_variables(vars_to_check, protocol_variables)


@pytest.mark.parametrize(
    "invalid",
    (
        [
            "Atmospheric|Concentrations|CH4",
            "Radiative|Forcing|Anthropogenic|Aerosols|Aerosols-radiation Interactions|Biomass Burning|NH3",
        ],
        [
            "Atmospheric Concentrations|Other|SOx",
            "Ocean ph",
            "Radiative Forcing|Volcanic",
        ],
        [
            "Effective Radiative Forcing|Aerosols",
            "Effective Radiative Forcing|Aerosols|Direct Effect",
            "Radiative Forcing|Anthropogenic|Aerosols-radiation Interactions|Biomass Burning|NH3",
        ],
    ),
)
def test_check_variables_errors(invalid, submission_template_xlsx):
    protocol_variables = load_submission_template_definitions(
        submission_template_xlsx, "variables"
    )["variable"].tolist()

    valid = [
        "Atmospheric Concentrations|CH4",
        "Atmospheric Concentrations|CO2",
        "Radiative Forcing|Anthropogenic|Aerosols|Aerosols-radiation Interactions|Biomass Burning|NH3",
        "Ocean pH",
        "Radiative Forcing|Natural|Volcanic",
    ]

    vars_to_check = valid + invalid
    error_msg = re.escape(
        "Variables do not match the RCMIP protocol:\n{}".format(sorted(set(invalid)))
    )

    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_variables(vars_to_check, protocol_variables)


@pytest.mark.parametrize(
    "regions_to_check",
    (
        [
            "World",
            "World|Northern Hemisphere",
            "World|Southern Hemisphere",
            "World|R5.2ASIA",
            "World|R5.2OECD",
        ],
        ["World", "World|Northern Hemisphere", "World|Southern Hemisphere"],
        ["World|R5.2ASIA", "World|R5.2OECD"],
        None,
    ),
)
def test_check_regions(regions_to_check, submission_template_xlsx):
    protocol_regions = load_submission_template_definitions(
        submission_template_xlsx, "regions"
    )["region"].tolist()
    if regions_to_check is None:
        regions_to_check = protocol_regions

    validate_regions(regions_to_check, protocol_regions)


@pytest.mark.parametrize(
    "invalid",
    (
        ["Global", "World|R5ASIA"],
        ["World|Soutthern Hemisphere|Ocean", "World|Northern Hemisphere|Land"],
        ["World|Germany", "Brazil"],
    ),
)
def test_check_regions_errors(invalid, submission_template_xlsx):
    protocol_regions = load_submission_template_definitions(
        submission_template_xlsx, "regions"
    )["region"].tolist()

    valid = [
        "World",
        "World|Northern Hemisphere",
        "World|Southern Hemisphere",
        "World|R5.2ASIA",
        "World|R5.2OECD",
    ]

    regions_to_check = valid + invalid
    error_msg = re.escape(
        "Regions do not match the RCMIP protocol:\n{}".format(sorted(set(invalid)))
    )

    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_regions(regions_to_check, protocol_regions)


@pytest.mark.parametrize(
    "scenarios_to_check",
    (
        [
            "piControl",
            "esm-piControl",
            "1pctCO2",
            "1pctCO2-bgc",
            "esm-1pct-brch-750PgC",
            "esm-rcp85-allGHG",
        ],
        ["esm-ssp370-lowNTCF-allGHG", "esm-ssp370-lowNTCF-aerchemmip", "ssp370"],
        ["esm-bell-750PgC", "esm-pi-CO2pulse", "abrupt-4xCO2"],
        None,
    ),
)
def test_check_scenarios(scenarios_to_check, submission_template_xlsx):
    protocol_scenarios = load_submission_template_definitions(
        submission_template_xlsx, "scenarios"
    )["scenario_id"].tolist()
    if scenarios_to_check is None:
        scenarios_to_check = protocol_scenarios

    validate_scenarios(scenarios_to_check, protocol_scenarios)


@pytest.mark.parametrize(
    "invalid",
    (["PiControl", "ESM-piControl"], ["Historical"], ["historical-CMIP5", "SSP119"],),
)
def test_check_scenarios_errors(invalid, submission_template_xlsx):
    protocol_scenarios = load_submission_template_definitions(
        submission_template_xlsx, "scenarios"
    )["scenario_id"].tolist()

    valid = [
        "piControl",
        "esm-piControl",
        "1pctCO2",
        "1pctCO2-bgc",
        "esm-1pct-brch-750PgC",
        "esm-rcp85-allGHG",
    ]

    scenarios_to_check = valid + invalid
    error_msg = re.escape(
        "scenarios do not match the RCMIP protocol:\n{}".format(sorted(set(invalid)))
    )

    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_scenarios(scenarios_to_check, protocol_scenarios)


@pytest.mark.parametrize(
    "variable_unit_combinations",
    (
        [
            ("Emissions|CO2", "MtCO2/yr"),
            ("Effective Radiative Forcing|Anthropogenic|Other", "W/m^2"),
            ("Heat Uptake", "J/s"),
            ("Carbon Pool|Atmosphere", "gC"),
        ],
        [
            ("Emissions|CO2", "MtCO2 / yr"),
            ("Effective Radiative Forcing|Anthropogenic|Other", "kW/m^2"),
            ("Heat Uptake", "J/yr"),
            ("Carbon Pool|Atmosphere", "kgC"),
        ],
        [
            ("Emissions|CO2", "GtC/yr"),
            ("Effective Radiative Forcing|Anthropogenic|Other", "W/m^2"),
            ("Heat Uptake", "ZJ/yr"),
            ("Carbon Pool|Atmosphere", "GtC"),
        ],
        [("Emissions|NOx", "Mt N/yr")],
    ),
)
def test_convert_units(variable_unit_combinations, submission_template_xlsx):
    start = ScmRun(
        data=np.arange(3 * len(variable_unit_combinations)).reshape(
            3, len(variable_unit_combinations)
        ),
        index=range(2000, 2021, 10),
        columns={
            "scenario": "ssp119",
            "model": "unspecified",
            "variable": [v[0] for v in variable_unit_combinations],
            "unit": [v[1] for v in variable_unit_combinations],
            "region": "World",
            "climate_model": "test_model v1.2.3",
        },
    )

    protocol_variables = load_submission_template_definitions(
        submission_template_xlsx, "variables"
    )

    res = convert_units_to_rcmip_units(start, protocol_variables)
    assert "unit_context" not in res.meta

    for v, _ in variable_unit_combinations:
        expected_unit = protocol_variables[protocol_variables["variable"] == v][
            "unit"
        ].iloc[0]
        assert (
            res.filter(variable=v).get_unique_meta("unit", no_duplicates=True)
            == expected_unit
        )


@pytest.mark.parametrize(
    "variable_unit_combinations",
    (
        [("Emissions|CO2", "MtCH4/yr")],
        [("Heat Uptake", "W/m^2")],
        [("Carbon Pool|Atmosphere", "GtCH4")],
        [("Carbon Pool|Atmosphere", "junk")],
    ),
)
def test_convert_units_errors(variable_unit_combinations, submission_template_xlsx):
    start = ScmRun(
        data=np.arange(3 * len(variable_unit_combinations)).reshape(
            3, len(variable_unit_combinations)
        ),
        index=range(2000, 2021, 10),
        columns={
            "scenario": "ssp119",
            "model": "unspecified",
            "variable": [v[0] for v in variable_unit_combinations],
            "unit": [v[1] for v in variable_unit_combinations],
            "region": "World",
            "climate_model": "test_model v1.2.3",
        },
    )

    protocol_variables = load_submission_template_definitions(
        submission_template_xlsx, "variables"
    )

    with pytest.raises(ProtocolConsistencyError):
        convert_units_to_rcmip_units(start, protocol_variables)


def test_convert_units_errors_some_valid(submission_template_xlsx):
    start = ScmRun(
        data=np.arange(12).reshape(3, 4),
        index=range(2000, 2021, 10),
        columns={
            "scenario": "ssp119",
            "model": "unspecified",
            "variable": [
                "Emissions|CO2",
                "Emissions|CH4",
                "Emissions|Sulfur",
                "Junk var",
            ],
            "unit": ["GtC / day", "MtC / yr", "Mt Sulfur / day", "m / s"],
            "region": "World",
            "climate_model": "test_model v1.2.3",
        },
    )

    protocol_variables = load_submission_template_definitions(
        submission_template_xlsx, "variables"
    )

    error_msg = re.escape(
        "Could not convert submission to protocol units. Errors:\n{}".format(
            pd.DataFrame(
                [
                    {
                        "variable": "Emissions|CH4",
                        "input_unit": "MtC / yr",
                        "protocol_unit": "Mt CH4/yr",
                        "exc_type": "<class 'pint.errors.DimensionalityError'>",
                        "exc_message": "Cannot convert from 'megatC / a' ([carbon] * [mass] / [time]) to 'CH4 * megametric_ton / a' ([mass] * [methane] / [time])",
                    },
                    {
                        "variable": "Emissions|Sulfur",
                        "input_unit": "Mt Sulfur / day",
                        "protocol_unit": "Mt SO2/yr",
                        "exc_type": "<class 'pint.errors.UndefinedUnitError'>",
                        "exc_message": "'Sulfur' is not defined in the unit registry",
                    },
                    {
                        "variable": "Junk var",
                        "input_unit": "m / s",
                        "protocol_unit": "None",
                        "exc_type": "<class 'IndexError'>",
                        "exc_message": "single positional indexer is out-of-bounds",
                    },
                ]
            )
        )
    )
    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        convert_units_to_rcmip_units(start, protocol_variables)


def test_validate_submission_model_reported_metrics(valid_model_reported):
    res = validate_submission_model_reported_metrics(valid_model_reported)

    pdt.assert_frame_equal(res, valid_model_reported)


def test_validate_submission_model_reported_metrics_wrong_columns(valid_model_reported):
    invalid = valid_model_reported.copy()
    invalid["extra_col"] = "hi"

    error_msg = "Input columns: {.*'extra_col'.*}. " "Expected columns: {.*}."
    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_submission_model_reported_metrics(invalid)


def test_validate_submission_model_reported_metrics_ensemble_member_not_int(
    valid_model_reported,
):
    invalid = valid_model_reported.copy()
    invalid["ensemble_member"] += 0.5

    error_msg = "`ensemble_member` column must be integers"
    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_submission_model_reported_metrics(invalid)


def test_validate_submission_model_reported_unrecognised_metric(valid_model_reported):
    invalid = valid_model_reported.copy()
    invalid["RCMIP name"] = "ECS"

    error_msg = (
        "The `RCMIP name` column should only contain 'Equilibrium Climate Sensitivity"
    )
    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_submission_model_reported_metrics(invalid)


def test_validate_submission_model_reported_invalid_unit(valid_model_reported):
    invalid = valid_model_reported.copy()
    invalid["unit"] = "delta_degC"

    error_msg = "values must be provided in 'K'"
    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_submission_model_reported_metrics(invalid)


@pytest.fixture()
def valid_model_meta():
    return pd.DataFrame(
        [
            {
                "climate_model": "MAGICC7.1.0",
                "climate_model_name": "MAGICC",
                "climate_model_version": "v7.1.0",
                "climate_model_configuration_label": "rcmip-phase-2-probabilistic",
                "climate_model_configuration_description": "RCMIP phase 2 probabilistic setup",
                "project": "None",
                "name_of_person": "ZN, MM, JL",
                "literature_reference": "Meinshausen et al., 2020",
            },
            {
                "climate_model": "climate_model",
                "climate_model_name": "climate_model_name",
                "climate_model_version": "climate_model_version",
                "climate_model_configuration_label": "climate_model_configuration_label",
                "climate_model_configuration_description": "climate_model_configuration_description",
                "project": "project",
                "name_of_person": "name_of_person",
                "literature_reference": "literature_reference",
            },
        ]
    )


def test_validate_submission_model_meta(valid_model_meta):
    res = validate_submission_model_meta(valid_model_meta)

    pdt.assert_frame_equal(res, valid_model_meta)


def test_validate_submission_model_meta_wrong_columns(valid_model_meta):
    invalid = valid_model_meta.copy()
    invalid["extra_col"] = "hi"

    error_msg = "Input columns: {.*'extra_col'.*}. " "Expected columns: {.*}."
    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_submission_model_meta(invalid)
