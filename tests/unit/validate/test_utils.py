import re

import pytest

from pyrcmip.validate.utils import load_submission_template_definitions


def assert_single_value_matches(inp, exp):
    assert len(inp) == 1
    assert inp.iloc[0] == exp


def test_load_submission_template_definitions_default_fp():
    res = load_submission_template_definitions(component="variables")

    assert set(res.columns) == {"category", "variable", "unit", "definition", "tier"}

    assert_single_value_matches(
        res[res["variable"] == "Atmospheric Concentrations|CH4"]["unit"], "ppb"
    )
    assert_single_value_matches(
        res[res["variable"] == "Atmospheric Concentrations|F-Gases|HFC"]["unit"], "ppm"
    )
    assert_single_value_matches(
        res[res["variable"] == "Radiative Forcing|Natural|Volcanic"]["unit"], "W/m^2"
    )


def test_load_submission_template_definitions_variables(submission_template_xlsx):
    res = load_submission_template_definitions(submission_template_xlsx, "variables")

    assert set(res.columns) == {"category", "variable", "unit", "definition", "tier"}

    assert_single_value_matches(
        res[res["variable"] == "Atmospheric Concentrations|CH4"]["definition"],
        "test definition",
    )
    assert_single_value_matches(
        res[res["variable"] == "Atmospheric Concentrations|CH4"]["unit"], "ppb"
    )
    assert_single_value_matches(
        res[res["variable"] == "Atmospheric Concentrations|F-Gases|HFC"]["unit"], "ppm"
    )
    assert_single_value_matches(
        res[res["variable"] == "Radiative Forcing|Natural|Volcanic"]["unit"], "W/m^2"
    )


def test_load_submission_template_definitions_regions_default_fp():
    res = load_submission_template_definitions(component="regions")

    assert set(res.columns) == {"category", "region", "definition"}

    assert_single_value_matches(res[res["region"] == "World"]["category"], "All")
    assert_single_value_matches(
        res[res["region"] == "World|Northern Hemisphere"]["category"], "Hemispheric"
    )
    assert_single_value_matches(
        res[res["region"] == "World|R5.2OECD"]["category"], "R5.2"
    )


def test_load_submission_template_definitions_regions(submission_template_xlsx):
    res = load_submission_template_definitions(submission_template_xlsx, "regions")

    assert set(res.columns) == {"category", "region", "definition"}

    assert_single_value_matches(
        res[res["region"] == "World"]["definition"], "test definition"
    )
    assert_single_value_matches(res[res["region"] == "World"]["category"], "All")
    assert_single_value_matches(
        res[res["region"] == "World|Northern Hemisphere"]["category"], "Hemispheric"
    )
    assert_single_value_matches(
        res[res["region"] == "World|R5.2OECD"]["category"], "R5.2"
    )


def test_load_submission_template_definitions_scenarios_default_fp(
    submission_template_xlsx,
):
    res = load_submission_template_definitions(component="scenarios")

    assert set(res.columns) == {"scenario_id", "description", "specification", "tier"}

    assert (
        res[res["scenario_id"] == "piControl"]["description"].str.startswith(
            "pre-industrial control simulation"
        )
    ).all()
    assert_single_value_matches(res[res["scenario_id"] == "1pctCO2-bgc"]["tier"], 3)
    assert_single_value_matches(
        res[res["scenario_id"] == "esm-ssp585-allGHG"]["tier"], 1
    )
    assert_single_value_matches(
        res[res["scenario_id"] == "esm-rcp85-allGHG"]["tier"], 3
    )


def test_load_submission_template_definitions_scenarios(submission_template_xlsx):
    res = load_submission_template_definitions(submission_template_xlsx, "scenarios")

    assert set(res.columns) == {"scenario_id", "description", "specification", "tier"}

    assert_single_value_matches(
        res[res["scenario_id"] == "piControl"]["description"], "test description"
    )
    assert_single_value_matches(res[res["scenario_id"] == "1pctCO2-bgc"]["tier"], 1)
    assert_single_value_matches(
        res[res["scenario_id"] == "esm-ssp585-allGHG"]["tier"], 2
    )
    assert_single_value_matches(
        res[res["scenario_id"] == "esm-rcp85-allGHG"]["tier"], 2
    )


def test_load_submission_template_definitions_unrecognised_component(
    submission_template_xlsx,
):
    error_msg = re.escape("Unrecognised component: junk")
    with pytest.raises(NotImplementedError, match=error_msg):
        load_submission_template_definitions(submission_template_xlsx, "junk")
