# Tests to move to scmdata
import numpy as np
import pytest
from scmdata import ScmRun

from pyrcmip.metric_calculations.utils import _time_mean


@pytest.mark.parametrize("variable", (None, "new_name"))
def test_time_mean(variable):
    start = ScmRun(
        np.arange(6).reshape(3, 2),
        [2010, 2020, 2030],
        columns={
            "scenario": "scenario",
            "model": ["model_a", "model_b"],
            "variable": "variable",
            "unit": "unit",
            "region": "region",
        },
    )

    res = _time_mean(start, variable=variable)
    assert res.name == "value"
    assert (res == [2, 3]).all()
    assert set(sorted(res.index.names)) == {
        "model",
        "region",
        "scenario",
        "unit",
        "variable",
        "evaluation_period_start_year",
        "evaluation_period_end_year",
    }
    if variable is None:
        assert (res.index.get_level_values("variable") == "variable").all()
    else:
        assert (res.index.get_level_values("variable") == variable).all()
