import logging
import re

import numpy as np
import numpy.testing as npt
import pytest
import scmdata

from pyrcmip.assessed_ranges import AssessedRanges
from pyrcmip.metric_calculations import (
    CalculatorAirborneFraction18501920,
    CalculatorAirborneFraction18501990,
)


@pytest.mark.parametrize(
    "calculator,start_year,end_year",
    (
        (CalculatorAirborneFraction18501920, 1850, 1920),
        (CalculatorAirborneFraction18501990, 1850, 1990),
    ),
)
def test_airborne_fraction(calculator, start_year, end_year, assessed_ranges_sample_df):
    n_years = end_year - start_year

    atmos_carbon_pool_delta = 600 * n_years / 70
    ocean_flux = 3 * (n_years / 70) ** 0.5
    land_flux = 2 * (n_years / 70) ** 0.5

    ocean_cumulative_uptake = ocean_flux * n_years
    land_cumulative_uptake = land_flux * n_years
    exp_af = atmos_carbon_pool_delta / (
        atmos_carbon_pool_delta + ocean_cumulative_uptake + land_cumulative_uptake
    )

    time = np.arange(1600, 2250)

    atmos_carbon_in = 550 * np.ones_like(time, dtype="float64")
    atmos_carbon_in[end_year - time[0] :] += atmos_carbon_pool_delta

    ocean_flux_in = ocean_flux * np.ones_like(time, dtype="float64")
    land_flux_in = land_flux * np.ones_like(time, dtype="float64")

    inp = scmdata.ScmRun(
        np.vstack([atmos_carbon_in, ocean_flux_in, land_flux_in]).T,
        index=time,
        columns={
            "scenario": "1pctCO2",
            "variable": [
                "Carbon Pool|Atmosphere",
                "Net Atmosphere to Ocean Flux|CO2",
                "Net Atmosphere to Land Flux|CO2",
            ],
            "unit": ["GtC", "GtC / yr", "GtC / yr"],
            "model": " unspecified",
            "region": "World",
            "climate_model": "test",
        },
    )

    ars = AssessedRanges(assessed_ranges_sample_df)
    (norm_period, evaluation_period) = ars.get_norm_period_evaluation_period(
        "Airborne Fraction|CO2 World 1pctCO2 1850-{}".format(calculator._end_year)
    )
    res = calculator.calculate_metric(
        ars, inp, norm_period, evaluation_period, "dimensionless"
    )

    npt.assert_allclose(res, exp_af, rtol=1e-4)


def test_airborne_fraction_land_ocean_combo_climate_model_missing(
    assessed_ranges_sample_df, caplog
):
    end_year = CalculatorAirborneFraction18501990._end_year
    start_year = CalculatorAirborneFraction18501990._start_year
    n_years = end_year - start_year

    atmos_carbon_pool_delta = 600 * n_years / 70
    time = np.arange(1600, 2250)
    atmos_carbon_in = 550 * np.ones_like(time, dtype="float64")
    atmos_carbon_in[end_year - time[0] :] += atmos_carbon_pool_delta

    inps = []
    exp_afs = {}
    for (ocean_flux, land_flux, ocean_and_land_flux, name) in (
        (3, 2, None, "model_a"),
        (None, None, 5.2, "model_b"),
        (None, None, None, "model_c"),
    ):
        cm_dat = [atmos_carbon_in]
        cm_vars = ["Carbon Pool|Atmosphere"]
        cm_units = ["GtC"]

        if (
            ocean_flux is not None and land_flux is not None
        ) or ocean_and_land_flux is not None:
            if ocean_and_land_flux is not None:
                ocean_and_land_cumulative_uptake = ocean_and_land_flux * n_years

                ocean_and_land_flux_in = ocean_and_land_flux * np.ones_like(
                    time, dtype="float64"
                )

                cm_dat += [ocean_and_land_flux_in]
                cm_vars += ["Net Atmosphere to Ocean and Land Flux|CO2"]
                cm_units += ["GtC / yr"]

            else:
                ocean_cumulative_uptake = ocean_flux * n_years
                land_cumulative_uptake = land_flux * n_years
                ocean_and_land_cumulative_uptake = (
                    ocean_cumulative_uptake + land_cumulative_uptake
                )

                ocean_flux_in = ocean_flux * np.ones_like(time, dtype="float64")
                land_flux_in = land_flux * np.ones_like(time, dtype="float64")

                cm_dat += [ocean_flux_in, land_flux_in]
                cm_vars += [
                    "Net Atmosphere to Ocean Flux|CO2",
                    "Net Atmosphere to Land Flux|CO2",
                ]
                cm_units += ["GtC / yr", "GtC / yr"]

            exp_afs[name] = atmos_carbon_pool_delta / (
                atmos_carbon_pool_delta + ocean_and_land_cumulative_uptake
            )

        inp = scmdata.ScmRun(
            np.vstack(cm_dat).T,
            index=time,
            columns={
                "scenario": "1pctCO2",
                "variable": cm_vars,
                "unit": cm_units,
                "model": " unspecified",
                "region": "World",
                "climate_model": name,
            },
        )
        inps.append(inp)

    inps = scmdata.run_append(inps)

    ars = AssessedRanges(assessed_ranges_sample_df)
    (norm_period, evaluation_period) = ars.get_norm_period_evaluation_period(
        "Airborne Fraction|CO2 World 1pctCO2 1850-1990"
    )

    with caplog.at_level(logging.INFO):
        res = CalculatorAirborneFraction18501990.calculate_metric(
            ars, inps, norm_period, evaluation_period, "dimensionless"
        )

    assert len(caplog.records) == 3
    assert caplog.records[0].msg == "Using separate land and ocean fluxes for `model_a`"
    assert caplog.records[0].levelname == "INFO"
    assert caplog.records[1].msg == "Using combined land and ocean fluxes for `model_b`"
    assert caplog.records[1].levelname == "INFO"
    assert caplog.records[2].msg == "No land or ocean fluxes for `model_c`"
    assert caplog.records[2].levelname == "WARNING"

    for name, series in res.groupby("climate_model"):
        npt.assert_allclose(series, exp_afs[name], rtol=1e-4)


def test_af_wrong_evaluation_period():
    error_msg = re.escape("Evaluation period other than [1990], input: [1921]")
    with pytest.raises(NotImplementedError, match=error_msg):
        CalculatorAirborneFraction18501990.calculate_metric(
            "a", "a", [1850], [1921], "K"
        )


def test_af_wrong_norm_period():
    error_msg = re.escape("Normalisation period other than [1850], input: [1750]")
    with pytest.raises(NotImplementedError, match=error_msg):
        CalculatorAirborneFraction18501990.calculate_metric(
            "a", "a", [1750], [1990], "K"
        )
