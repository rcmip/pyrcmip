import re

import numpy as np
import numpy.testing as npt
import pytest
import scmdata

from pyrcmip.assessed_ranges import AssessedRanges
from pyrcmip.metric_calculations import CalculatorTCR


def test_tcr(assessed_ranges_sample_df):
    exp_tcr = 1.98
    time = np.arange(1600, 2250)

    temps_in = np.zeros_like(time, dtype="float64")
    # still works even if only a single year is non-zero
    # could add more checking in future
    temps_in[1920 - time[0]] = exp_tcr

    inp = scmdata.ScmRun(
        temps_in,
        index=time,
        columns={
            "scenario": "1pctCO2",
            "variable": "Surface Air Temperature Change",
            "unit": "K",
            "model": " unspecified",
            "region": "World",
        },
    )

    ars = AssessedRanges(assessed_ranges_sample_df)
    (norm_period, evaluation_period) = ars.get_norm_period_evaluation_period(
        "Transient Climate Response"
    )
    res = CalculatorTCR.calculate_metric(ars, inp, norm_period, evaluation_period, "K")

    npt.assert_allclose(res, exp_tcr)


def test_tcr_wrong_evaluation_period():
    error_msg = re.escape("Evaluation period other than [1920], input: [1921]")
    with pytest.raises(NotImplementedError, match=error_msg):
        CalculatorTCR.calculate_metric("a", "a", [1850], [1921], "K")


def test_tcr_wrong_norm_period():
    error_msg = re.escape("Normalisation period other than [1850], input: [1750]")
    with pytest.raises(NotImplementedError, match=error_msg):
        CalculatorTCR.calculate_metric("a", "a", [1750], [1920], "K")
