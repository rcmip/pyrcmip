import logging
import re

import numpy as np
import numpy.testing as npt
import pytest
import scmdata

from pyrcmip.assessed_ranges import AssessedRanges
from pyrcmip.metric_calculations import CalculatorTCRE


def test_tcre(assessed_ranges_sample_df):
    exp_tcr = 1.98
    exp_cumulative_co2 = 1000
    exp_tcre = exp_tcr / exp_cumulative_co2

    time = np.arange(1600, 2250)

    temps_in = np.zeros_like(time, dtype="float64")
    cumulative_emms_in = np.zeros_like(time, dtype="float64")
    # still works even if only a single year is non-zero
    # could add more checking in future
    temps_in[1920 - time[0]] = exp_tcr
    cumulative_emms_in[1920 - time[0]] = exp_cumulative_co2

    inp = scmdata.ScmRun(
        np.vstack([temps_in, cumulative_emms_in]).T,
        index=time,
        columns={
            "scenario": "1pctCO2",
            "variable": ["Surface Air Temperature Change", "Cumulative Emissions|CO2"],
            "unit": ["K", "GtC"],
            "model": " unspecified",
            "region": "World",
            "climate_model": "test",
        },
    )

    ars = AssessedRanges(assessed_ranges_sample_df)
    (norm_period, evaluation_period) = ars.get_norm_period_evaluation_period(
        "Transient Climate Response to Emissions"
    )
    res = CalculatorTCRE.calculate_metric(
        ars, inp, norm_period, evaluation_period, "K / GtC"
    )

    npt.assert_allclose(res, exp_tcre)


def test_tcre_climate_model_missing(assessed_ranges_sample_df, caplog):
    inps = []
    exp_tcres = {}
    for (tcr, cumulative_co2, name) in (
        (1.98, 1100, "model_a"),
        (1.6, 998, "model_b"),
        (1.98, None, "model_c"),
    ):
        common_cols = {
            "scenario": "1pctCO2",
            "model": " unspecified",
            "region": "World",
            "climate_model": name,
        }
        time = np.arange(1600, 2250)
        temps_in = np.zeros_like(time, dtype="float64")
        temps_in[1920 - time[0]] = tcr
        temps_in_scmrun = scmdata.ScmRun(
            temps_in,
            index=time,
            columns={
                "variable": "Surface Air Temperature Change",
                "unit": "K",
                **common_cols,
            },
        )
        inps.append(temps_in_scmrun)

        if cumulative_co2 is not None:
            exp_tcres[name] = tcr / cumulative_co2
            cumulative_emms_in = np.zeros_like(time, dtype="float64")
            cumulative_emms_in[1920 - time[0]] = cumulative_co2
            cumulative_co2_scmrun = scmdata.ScmRun(
                cumulative_emms_in,
                index=time,
                columns={
                    "variable": "Cumulative Emissions|CO2",
                    "unit": "GtC",
                    **common_cols,
                },
            )
            inps.append(cumulative_co2_scmrun)

    inps = scmdata.run_append(inps)

    ars = AssessedRanges(assessed_ranges_sample_df)
    (norm_period, evaluation_period) = ars.get_norm_period_evaluation_period(
        "Transient Climate Response to Emissions"
    )

    with caplog.at_level(logging.WARNING):
        res = CalculatorTCRE.calculate_metric(
            ars, inps, norm_period, evaluation_period, "K / GtC"
        )

    assert len(caplog.records) == 1
    assert caplog.records[0].msg == "No Cumulative Emissions|CO2 for `model_c`"

    for name, series in res.groupby("climate_model"):
        npt.assert_allclose(series, exp_tcres[name])


def test_tcr_wrong_evaluation_period():
    error_msg = re.escape("Evaluation period other than [1920], input: [1921]")
    with pytest.raises(NotImplementedError, match=error_msg):
        CalculatorTCRE.calculate_metric("a", "a", [1850], [1921], "K")


def test_tcr_wrong_norm_period():
    error_msg = re.escape("Normalisation period other than [1850], input: [1750]")
    with pytest.raises(NotImplementedError, match=error_msg):
        CalculatorTCRE.calculate_metric("a", "a", [1750], [1920], "K")
