import re
from os.path import join

import pytest


@pytest.mark.parametrize(
    "extra,err_msg",
    (
        (["data/does_not_exist"], r"Missing argument 'TIMESERIES"),
        (
            ["rcmip_model_output_test.nc", "rcmip_model_reported_metrics_test.csv"],
            r"Missing argument 'TIMESERIES",
        ),
        (
            [
                "data/does_not_exist",
                "rcmip_model_reported_metrics_test.csv",
                r"rcmip_model_metadata_test.csv",
            ],
            r"Invalid value for 'TIMESERIES.*data/does_not_exist' does not exist.",
        ),
        (
            [
                "rcmip_model_output_test.nc",
                "data/does_not_exist",
                "rcmip_model_metadata_test.csv",
            ],
            r"Invalid value for 'MODEL_REPORTED.*data/does_not_exist' does not exist.",
        ),
        (
            [
                "rcmip_model_output_test.nc",
                "rcmip_model_reported_metrics_test.csv",
                "data/does_not_exist",
            ],
            r"Invalid value for 'METADATA.*data/does_not_exist' does not exist.",
        ),
        (
            [
                "data/does_not_exist",
                "rcmip_model_output_test_ssps.nc",
                "rcmip_model_reported_metrics_test.csv",
                "data/does_not_exist",
            ],
            r"Invalid value for 'TIMESERIES.*data/does_not_exist' does not exist.",
        ),
    ),
)
def test_validate_missing(run_cli, test_data_dir, extra, err_msg):
    result = run_cli(["validate"] + [join(test_data_dir, e) for e in extra])
    assert result.exit_code == 2, result.output
    assert re.search(err_msg, result.output)


def test_validate_passing(run_cli, test_data_dir):
    result = run_cli(
        [
            "validate",
            join(test_data_dir, "rcmip_model_output_test.nc"),
            join(test_data_dir, "rcmip_model_reported_metrics_test.csv"),
            join(test_data_dir, "rcmip_model_metadata_test.csv"),
        ]
    )
    assert result.exit_code == 0, result.output
    assert "valid submission" in result.output


def test_validate_passing_multiple(run_cli, test_data_dir):
    result = run_cli(
        [
            "validate",
            join(test_data_dir, "rcmip_model_output_test_ssps.nc"),
            join(test_data_dir, "rcmip_model_output_test_nonssps.nc"),
            join(test_data_dir, "rcmip_model_reported_metrics_test.csv"),
            join(test_data_dir, "rcmip_model_metadata_test.csv"),
        ]
    )
    assert result.exit_code == 0, result.output
    assert "valid submission" in result.output


def test_validate_old_submission_template_failing(run_cli, test_data_dir, results_dir):
    result = run_cli(
        [
            "validate",
            join(results_dir, "acc2/rcmip_phase-1_acc2_v1-0-0.xlsx"),
            join(test_data_dir, "rcmip_model_reported_metrics_test.csv"),
            join(test_data_dir, "rcmip_model_metadata_test.csv"),
        ]
    )
    assert result.exit_code == 1
    assert "reading timeseries failed" in result.output
