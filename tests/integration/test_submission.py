# Tests to write:
# - can we process Su's data
# - can we rename variables etc.
# - can we save it as ScmRun in the right database
# - can it be submitted

# - checks in submission:
#   - includes model reported metrics
#   - climate_model consistent throughout
#   - file naming (automatically done when saving to database)
#   - variable naming
#   - units
#   - percentiles
#   - calculation of derived metrics (logging important here)

# - new plotting functionality:
#   - custom box and whisker plots
