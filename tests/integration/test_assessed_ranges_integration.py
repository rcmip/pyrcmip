import os.path
import re
from unittest.mock import MagicMock, call, patch

import matplotlib.axes
import matplotlib.cbook as cbook
import numpy as np
import pandas as pd
import pandas.testing as pdt
import pytest
import scmdata

import pyrcmip.metric_calculations
from pyrcmip.assessed_ranges import AssessedRanges


@pytest.fixture(scope="module")
def results_sample(test_data_dir):
    tfile = os.path.join(test_data_dir, "rcmip_model_output_test.nc")
    return scmdata.ScmRun.from_nc(tfile)


@pytest.mark.parametrize(
    "norm_period,evaluation_period,year_filter,exp_error,error_msg",
    (
        (range(1961, 1990 + 1), range(2010, 2019 + 1), None, False, None,),
        (
            range(1961, 1990 + 1),
            range(2010, 2019 + 1),
            range(1962, 2100),
            True,
            re.escape(
                "Res min. year: 1962 is greater than normalisation period min.: 1961"
            ),
        ),
        (
            range(1961, 1990 + 1),
            range(2010, 2019 + 1),
            range(1, 1985),
            True,
            re.escape(
                "Res max. year: 1984 is less than normalisation period max.: 1990"
            ),
        ),
        (
            range(1961, 1990 + 1),
            range(2010, 2019 + 1),
            range(1, 2010),
            True,
            re.escape("Res max. year: 2009 is less than evaluation period max.: 2019"),
        ),
        (
            None,
            range(2010, 2019 + 1),
            range(2015, 2100),
            True,
            re.escape(
                "Res min. year: 2015 is greater than evaluation period min.: 2010"
            ),
        ),
        (None, None, range(2001, 2002), False, None,),
    ),
)
def test_check_norm_period_evaluation_period_against_data(
    norm_period,
    evaluation_period,
    year_filter,
    exp_error,
    error_msg,
    assessed_ranges_sample,
    results_sample,
):
    if year_filter is not None:
        results_sample = results_sample.filter(year=year_filter)

    if exp_error:
        with pytest.raises(ValueError, match=error_msg):
            assessed_ranges_sample.check_norm_period_evaluation_period_against_data(
                norm_period, evaluation_period, results_sample
            )
    else:
        assessed_ranges_sample.check_norm_period_evaluation_period_against_data(
            norm_period, evaluation_period, results_sample
        )


def _make_cm_df(cm, metric, unit):
    values = np.random.random(500)
    ensemble_members = np.random.random(values.shape)

    return pd.DataFrame(
        [(v, cm, em, metric, unit) for v, em in zip(values, ensemble_members)],
        columns=["value", "climate_model", "ensemble_member", "RCMIP name", "unit"],
    )


@pytest.fixture
def model_results_sample():
    out = pd.concat(
        [
            _make_cm_df("MAGICC6", "Equilibrium Climate Sensitivity", "K"),
            _make_cm_df("MAGICC6", "Transient Climate Response", "K"),
            _make_cm_df(
                "MAGICC6", "Atmospheric Lifetime|CH4 World historical-2005", "yr"
            ),
            _make_cm_df("FaIR1.6", "Equilibrium Climate Sensitivity", "K"),
            _make_cm_df("FaIR1.6", "Transient Climate Response", "K"),
            _make_cm_df("MCE", "Equilibrium Climate Sensitivity", "K"),
            _make_cm_df("MCE", "Transient Climate Response", "K"),
            _make_cm_df("MCE", "Atmospheric Lifetime|CH4 World historical-2005", "yr"),
        ]
    )

    return out


@pytest.mark.parametrize(
    "metric",
    (
        "Equilibrium Climate Sensitivity",
        "Transient Climate Response",
        "Atmospheric Lifetime|CH4 World historical-2005",
    ),
)
def test_get_results_summary_table_for_metric(
    metric, assessed_ranges_sample, model_results_sample
):
    exp = {
        "climate_model": [],
        "assessed_range_label": [],
        "assessed_range_value": [],
        "climate_model_value": [],
        "percentage_difference": [],
    }

    assessed_range_quantiles = {
        "very_likely__lower": 0.05,
        "likely__lower": 0.17,
        "central": 0.5,
        "likely__upper": 0.83,
        "very_likely__upper": 0.95,
    }

    model_results_sample_metric = model_results_sample.loc[
        model_results_sample[assessed_ranges_sample.metric_column] == metric, :
    ]
    for label, quantile in assessed_range_quantiles.items():
        assessed_value = assessed_ranges_sample.get_col_for_metric(metric, label)
        if np.isnan(assessed_value):
            continue

        for climate_model, df in model_results_sample_metric.groupby("climate_model"):
            model_quantile = df["value"].quantile(quantile)

            exp["climate_model"].append(climate_model)
            exp["assessed_range_label"].append(label)
            exp["assessed_range_value"].append(assessed_value)
            exp["climate_model_value"].append(model_quantile)
            exp["percentage_difference"].append(
                (model_quantile - assessed_value) / np.abs(assessed_value) * 100
            )

    exp = pd.DataFrame(exp)
    exp[assessed_ranges_sample.metric_column] = metric
    exp["unit"] = assessed_ranges_sample.get_col_for_metric(metric, "unit")

    res = assessed_ranges_sample.get_results_summary_table_for_metric(
        metric, model_results_sample
    )

    pdt.assert_frame_equal(res, exp)


def test_get_results_summary_table_for_metric_no_metric(
    assessed_ranges_sample, model_results_sample
):
    error_msg = re.escape("No model results for metric: junk")
    with pytest.raises(KeyError, match=error_msg):
        assessed_ranges_sample.get_results_summary_table_for_metric(
            "junk", model_results_sample
        )


def test_get_results_summary_table_for_metric_mismatched_units(
    assessed_ranges_sample, model_results_sample
):
    model_results_sample.loc[
        model_results_sample[assessed_ranges_sample.metric_column]
        == "Equilibrium Climate Sensitivity",
        "unit",
    ] = "mK"

    error_msg = re.escape(
        "Units mismatch. Assessed range units: K, model results unit(s): ['mK']"
    )
    with pytest.raises(AssertionError, match=error_msg):
        assessed_ranges_sample.get_results_summary_table_for_metric(
            "Equilibrium Climate Sensitivity", model_results_sample
        )


@pytest.mark.parametrize(
    "metric",
    (
        "Equilibrium Climate Sensitivity",
        "Transient Climate Response",
        "Atmospheric Lifetime|CH4 World historical-2005",
    ),
)
def test_get_box_plot_df(metric, assessed_ranges_sample, model_results_sample):
    assessed_ranges_df = assessed_ranges_sample.get_assessed_range_for_boxplot(metric)

    model_results_sample_metric = (
        model_results_sample.loc[
            model_results_sample[assessed_ranges_sample.metric_column] == metric, :
        ]
        .pivot_table(
            values="value",
            columns=assessed_ranges_sample.metric_column,
            index=list(
                set(model_results_sample.columns)
                - {"value", assessed_ranges_sample.metric_column}
            ),
        )
        .reset_index()
    )
    model_results_sample_metric["Source"] = model_results_sample_metric["climate_model"]

    exp = pd.concat([model_results_sample_metric, assessed_ranges_df])[
        [metric, "Source", "unit"]
    ]

    # make sure random draws inget_assessed_range_for_boxplot don't cause failures
    with patch.object(
        assessed_ranges_sample,
        "get_assessed_range_for_boxplot",
        return_value=assessed_ranges_df,
    ):
        res = assessed_ranges_sample._get_box_plot_df(metric, model_results_sample)

    pdt.assert_frame_equal(
        res.sort_values(["Source", metric]).reset_index(drop=True),
        exp.sort_values(["Source", metric]).reset_index(drop=True),
    )


def test_get_box_plot_df_non_unique_units_raises(
    assessed_ranges_sample, model_results_sample
):
    metric = "Equilibrium Climate Sensitivity"
    model_results_sample["unit"] = "mK"

    error_msg = re.escape("Not a single unit, found: ['K', 'mK']")
    with pytest.raises(AssertionError, match=error_msg):
        assessed_ranges_sample._get_box_plot_df(metric, model_results_sample)


@pytest.mark.parametrize("box_quantiles", ((17, 83), (33, 67)))
@pytest.mark.parametrize("whisker_quantiles", ((5, 95), (10, 90)))
@pytest.mark.parametrize(
    "metric",
    (
        "Equilibrium Climate Sensitivity",
        "Transient Climate Response",
        "Atmospheric Lifetime|CH4 World historical-2005",
    ),
)
def test_get_box_whisker_stats_custom_quantiles(
    assessed_ranges_sample,
    model_results_sample,
    metric,
    whisker_quantiles,
    box_quantiles,
):
    assessed_ranges_df = assessed_ranges_sample.get_assessed_range_for_boxplot(metric)

    model_results_sample_metric = (
        model_results_sample.loc[
            model_results_sample[assessed_ranges_sample.metric_column] == metric, :
        ]
        .pivot_table(
            values="value",
            columns=assessed_ranges_sample.metric_column,
            index=list(
                set(model_results_sample.columns)
                - {"value", assessed_ranges_sample.metric_column}
            ),
        )
        .reset_index()
    )
    model_results_sample_metric["Source"] = model_results_sample_metric["climate_model"]

    plot_df = pd.concat([model_results_sample_metric, assessed_ranges_df])[
        [metric, "Source", "unit"]
    ]

    exp = []
    for source, df in plot_df.groupby("Source"):
        values = df[metric].values.squeeze()

        source_dict = cbook.boxplot_stats(values, labels=[source])[0]

        if source == assessed_ranges_sample.assessed_range_label:
            ar_values = assessed_ranges_sample._get_assessed_range_values_for_metric(
                metric
            )

            if not np.isnan(ar_values["central"]):
                source_dict["med"] = ar_values["central"]
            else:
                source_dict["med"] = np.percentile(values, 50)

            if not np.isnan(ar_values["likely__lower"]):
                source_dict["q1"], source_dict["q3"] = (
                    ar_values["likely__lower"],
                    ar_values["likely__upper"],
                )
            else:
                source_dict["q1"], source_dict["q3"] = np.percentile(
                    values, box_quantiles
                )

            if not np.isnan(ar_values["very_likely__lower"]):
                source_dict["whislo"], source_dict["whishi"] = (
                    ar_values["very_likely__lower"],
                    ar_values["very_likely__upper"],
                )
            else:
                source_dict["whislo"], source_dict["whishi"] = np.percentile(
                    values, whisker_quantiles
                )

        else:
            source_dict["q1"], source_dict["q3"] = np.percentile(values, box_quantiles)
            source_dict["whislo"], source_dict["whishi"] = np.percentile(
                values, whisker_quantiles
            )

        exp.append(source_dict)

    res = assessed_ranges_sample._get_box_whisker_stats_custom_quantiles(
        plot_df,
        metric,
        box_quantiles=box_quantiles,
        whisker_quantiles=whisker_quantiles,
    )

    assert len(res) == len(exp)
    for v in exp:
        label = v["label"]
        r = [r for r in res if r["label"] == label][0]

        for k in ["med", "q1", "q3", "whislo", "whishi"]:
            assert v[k] == r[k]


def test_plot_box_only(assessed_ranges_sample, model_results_sample):
    metric = "Equilibrium Climate Sensitivity"
    mock_ax = MagicMock()

    box_plot_stats = [{"stats": "list of dicts which has been mocked"}]
    box_plot_df = assessed_ranges_sample._get_box_plot_df(metric, model_results_sample)

    with patch.object(
        assessed_ranges_sample, "_get_box_plot_df", return_value=box_plot_df
    ) as mock_get_box_plot_df:

        with patch.object(
            assessed_ranges_sample,
            "_get_box_whisker_stats_custom_quantiles",
            return_value=box_plot_stats,
        ) as mock_get_box_whisker_stats_custom_quantiles:

            res = assessed_ranges_sample._plot(
                metric, model_results_sample, mock_ax, box_only=True
            )

            mock_get_box_plot_df.assert_called_with(metric, model_results_sample)

            mock_get_box_whisker_stats_custom_quantiles.assert_called_with(
                box_plot_df, metric, box_quantiles=(17, 83), whisker_quantiles=(5, 95)
            )

    assert res == mock_ax

    mock_ax.bxp.assert_called_with(box_plot_stats, showfliers=False, vert=True)
    mock_ax.set_ylabel.assert_called_with("K")
    mock_ax.set_title.assert_called_with(metric)
    mock_ax.grid.assert_called_with(axis="y")


@patch("pyrcmip.assessed_ranges.sns")
def test_plot_box_and_dist(mock_sns, assessed_ranges_sample, model_results_sample):
    metric = "Equilibrium Climate Sensitivity"
    mock_axs = [MagicMock(), MagicMock()]

    box_plot_stats = [{"stats": "list of dicts which has been mocked"}]
    box_plot_df = assessed_ranges_sample._get_box_plot_df(metric, model_results_sample)

    tpalette = {
        "MCE": "tab:red",
        "MAGICC6": "tab:green",
        "FaIR1.6": "tab:blue",
        assessed_ranges_sample.assessed_range_label: "tab:gray",
    }
    with patch.object(
        assessed_ranges_sample, "_get_box_plot_df", return_value=box_plot_df
    ) as mock_get_box_plot_df:

        with patch.object(
            assessed_ranges_sample,
            "_get_box_whisker_stats_custom_quantiles",
            return_value=box_plot_stats,
        ) as mock_get_box_whisker_stats_custom_quantiles:

            res = assessed_ranges_sample._plot(
                metric, model_results_sample, mock_axs, box_only=False, palette=tpalette
            )

            mock_get_box_plot_df.assert_called_with(metric, model_results_sample)

            mock_get_box_whisker_stats_custom_quantiles.assert_called_with(
                box_plot_df, metric, box_quantiles=(17, 83), whisker_quantiles=(5, 95)
            )

    assert res == mock_axs

    mock_axs[1].bxp.assert_called_with(box_plot_stats, showfliers=False, vert=False)
    mock_axs[1].set_xlabel.assert_called_with("K")
    mock_axs[1].set_title.assert_called_with("")
    mock_axs[1].grid.assert_called_with(axis="x")

    assert mock_sns.histplot.called_with(
        data=box_plot_df,
        x=metric,
        bins=50,
        stat="probability",
        common_norm=False,
        kde=True,
        hue="Source",
        palette=tpalette,
        ax=mock_axs[0],
        legend=True,
    )

    mock_axs[0].set_title.assert_called_with(metric)
    mock_axs[0].set_xlabel.assert_called_with("")


def test_get_evaluation_axes(assessed_ranges_sample):
    res = assessed_ranges_sample._get_evaluation_axes()
    assert len(res) == 2
    for r in res:
        assert isinstance(r, matplotlib.axes.SubplotBase)


@pytest.mark.parametrize("axes", (None, (MagicMock(), MagicMock())))
@pytest.mark.parametrize("palette", (None, {"hi": "blue"}))
def test_plot_metric_and_results(
    assessed_ranges_sample, model_results_sample, axes, palette
):
    metric = "Equilibrium Climate Sensitivity"

    plot_return_val = 12
    get_axes_return_val = ["hi", "bye"]

    with patch.object(
        assessed_ranges_sample, "_plot", return_value=plot_return_val,
    ) as mock_plot:

        with patch.object(
            assessed_ranges_sample,
            "_get_evaluation_axes",
            return_value=get_axes_return_val,
        ) as mock_get_evaluation_axes:

            assessed_ranges_sample.plot_metric_and_results(
                metric, model_results_sample, axes=axes, palette=palette,
            )

    if axes is None:
        mock_get_evaluation_axes.assert_called()
        mock_plot.assert_called_with(
            metric,
            model_results_sample,
            axs=get_axes_return_val,
            box_only=False,
            palette=palette,
        )
    else:
        mock_get_evaluation_axes.assert_not_called()
        mock_plot.assert_called_with(
            metric, model_results_sample, axs=axes, box_only=False, palette=palette
        )


@pytest.mark.parametrize("axes", (None, MagicMock()))
@pytest.mark.parametrize("palette", (None, {"hi": "blue"}))
@patch("pyrcmip.assessed_ranges.plt")
def test_plot_metric_and_results_box_only(
    mock_plt, assessed_ranges_sample, model_results_sample, axes, palette
):
    metric = "Equilibrium Climate Sensitivity"

    plot_return_val = 12
    plt_return_val = "hi"
    mock_plt.figure().add_subplot.return_value = plt_return_val

    with patch.object(
        assessed_ranges_sample, "_plot", return_value=plot_return_val,
    ) as mock_plot:

        assessed_ranges_sample.plot_metric_and_results_box_only(
            metric, model_results_sample, ax=axes, palette=palette,
        )

    if axes is None:
        mock_plt.figure().add_subplot.assert_called()
        mock_plot.assert_called_with(
            metric,
            model_results_sample,
            axs=plt_return_val,
            box_only=True,
            palette=palette,
        )
    else:
        mock_plt.figure().add_subplot.assert_not_called()
        mock_plot.assert_called_with(
            metric, model_results_sample, axs=axes, box_only=True, palette=palette
        )


@pytest.fixture()
def scmrun_results_sample_expected_metrics():
    scmrun = []
    expected_metrics = []

    def _create_scm_run(
        data,
        index,
        variable,
        unit,
        ensemble_members,
        climate_model,
        scenario,
        region="World",
        model="unspecified",
    ):
        out = scmdata.ScmRun(
            data=data,
            index=index,
            columns={
                "climate_model": climate_model,
                "model": model,
                "scenario": scenario,
                "region": region,
                "variable": variable,
                "unit": unit,
                "ensemble_member": ensemble_members,
            },
        )

        return out

    def _create_expected_metric_df(
        scmrun,
        metric,
        unit,
        ensemble_member_values,
        variable=None,
        scenario=None,
        norm_period=None,
        eval_period=None,
    ):
        climate_model = scmrun.get_unique_meta("climate_model", no_duplicates=True)

        out = pd.DataFrame(
            [(climate_model, unit, em, v, metric) for em, v in ensemble_member_values],
            columns=["climate_model", "unit", "ensemble_member", "value", "RCMIP name"],
        )
        columns_to_copy = ["model", "region"]
        if variable is None:
            columns_to_copy.append("variable")
        else:
            out["variable"] = variable

        if scenario is None:
            columns_to_copy.append("scenario")
        else:
            out["scenario"] = scenario

        for c in columns_to_copy:
            out[c] = scmrun.get_unique_meta(c, no_duplicates=True)

        if norm_period is not None:
            out["reference_period_start_year"] = norm_period[0]
            out["reference_period_end_year"] = norm_period[1]

        if eval_period is not None:
            out["evaluation_period_start_year"] = eval_period[0]
            out["evaluation_period_end_year"] = eval_period[1]

        return out

    magicc_1pctco2_tas = _create_scm_run(
        np.vstack([1 * np.arange(251) / 70, 1.1 * np.arange(251) / 70]).T,
        range(1850, 2100 + 1),
        "Surface Air Temperature Change",
        "K",
        [0, 1],
        "MAGICC6",
        "1pctCO2",
        region="World",
        model="unspecified",
    )

    magicc_tcr = _create_expected_metric_df(
        magicc_1pctco2_tas,
        "Transient Climate Response",
        "K",
        ((0, 1), (1, 1.1)),
        norm_period=(1850, 1850),
        eval_period=(1920, 1920),
    )
    scmrun.append(magicc_1pctco2_tas)
    expected_metrics.append(magicc_tcr)

    scmrun.append(
        _create_scm_run(
            np.vstack([1000 * np.arange(251) / 70, 1000 * np.arange(251) / 70]).T,
            range(1850, 2100 + 1),
            "Cumulative Emissions|CO2",
            "GtC",
            [0, 1],
            "MAGICC6",
            "1pctCO2",
            region="World",
            model="unspecified",
        )
    )
    expected_metrics.append(
        _create_expected_metric_df(
            scmrun[-1],
            "Transient Climate Response to Emissions",
            "K / TtC",
            ((0, 1), (1, 1.1)),
            norm_period=(1850, 1850),
            eval_period=(1920, 1920),
            variable="Surface Air Temperature Change,Cumulative Emissions|CO2",
        )
    )

    scmrun.append(
        _create_scm_run(
            np.vstack(
                [
                    # add one to ensure no evaluation period is applied
                    1 + 10 * np.arange(251) / 160,
                    1 + 10.3 * np.arange(251) / 160,
                    1 + 100 * np.arange(251) / 160,
                    1 + 1.3 * np.arange(251) / 160,
                ]
            ).T,
            range(1850, 2100 + 1),
            "Atmospheric Lifetime|CH4",
            "yr",
            [0, 1, 0, 1],
            "MAGICC6",
            # shouldn't get confused by extra scenarios
            scenario=["historical", "historical", "ssp245", "ssp245"],
            region="World",
            model="unspecified",
        )
    )
    expected_metrics.append(
        _create_expected_metric_df(
            scmrun[-1],
            "Atmospheric Lifetime|CH4 World historical-2005",
            "yr",
            (
                (
                    0,
                    scmrun[-1]
                    .filter(
                        scenario="historical",
                        ensemble_member=0,
                        year=range(2005, 2014 + 1),
                    )
                    .values.mean(),
                ),
                (
                    1,
                    scmrun[-1]
                    .filter(
                        scenario="historical",
                        ensemble_member=1,
                        year=range(2005, 2014 + 1),
                    )
                    .values.mean(),
                ),
            ),
            eval_period=(2005, 2014),
            scenario="historical",
        )
    )

    heat_content_ts = np.arange(251)
    ref_val = 10
    check_val = 322
    heat_content_ts[1971 - 1850] = ref_val
    heat_content_ts[2018 - 1850] = check_val + ref_val
    scmrun.append(
        _create_scm_run(
            np.vstack([heat_content_ts, 300 * np.arange(251) / 160]).T,
            range(1850, 2100 + 1),
            "Heat Content|Ocean",
            "ZJ",
            [0, 1],
            "MAGICC6",
            scenario="ssp245",
            region="World",
            model="unspecified",
        )
    )
    expected_metrics.append(
        _create_expected_metric_df(
            scmrun[-1],
            "Heat Content|Ocean World ssp245 1971-2018",
            "ZJ",
            (
                (0, check_val),
                (
                    1,
                    scmrun[-1]
                    .relative_to_ref_period_mean(year=1971)
                    .filter(ensemble_member=1, year=range(2018, 2018 + 1),)
                    .values.mean(),
                ),
            ),
            eval_period=(2018, 2018),
            norm_period=(1971, 1971),
        )
    )

    funny_concs = np.arange(151)
    funny_concs[1980 - 1850 : 1985 - 1850] = np.arange(5)
    funny_concs[1985 - 1850 : 1990 - 1850] = np.arange(5)[::-1]
    scmrun.append(
        _create_scm_run(
            278
            + np.vstack([-0.98 * np.arange(151), 1.02 * np.arange(151), funny_concs]).T,
            range(1850, 2000 + 1),
            "Atmospheric Concentrations|CO2",
            "ppm",
            [0, 1, 2],
            "MAGICC6",
            "esm-hist",
            region="World",
            model="unspecified",
        )
    )
    expected_metrics.append(
        _create_expected_metric_df(
            scmrun[-1],
            "Rate Increase Atmospheric Concentrations|CO2 World esm-hist-1980",
            "ppm / yr",
            ((0, -0.98), (1, 1.02), (2, 0)),
            eval_period=(1980, 1989),
        )
    )

    scmrun.append(
        _create_scm_run(
            np.vstack(
                [
                    2 * np.arange(251) / 70,
                    1.7 * np.arange(251) / 70,
                    1.3 * np.arange(251) / 70,
                    0.9 * np.arange(251) / 70,
                    0.8 * np.arange(251) / 70,
                ]
            ).T,
            range(1850, 2100 + 1),
            "Surface Air Temperature Change",
            "K",
            [0, 1, 10, 11, 12],
            "FaIR1.6",
            "1pctCO2",
            region="World",
            model="unspecified",
        )
    )
    expected_metrics.append(
        _create_expected_metric_df(
            scmrun[-1],
            "Transient Climate Response",
            "K",
            ((0, 2), (1, 1.7), (10, 1.3), (11, 0.9), (12, 0.8)),
            norm_period=(1850, 1850),
            eval_period=(1920, 1920),
        )
    )

    scmrun.append(
        _create_scm_run(
            np.vstack(
                [
                    1 * np.arange(251) / 70,
                    1.1 * np.arange(251) / 70,
                    1.2 * np.arange(251) / 70,
                    1.3 * np.arange(251) / 70,
                    1.4 * np.arange(251) / 70,
                ]
            ).T,
            range(1850, 2100 + 1),
            "Cumulative Emissions|CO2",
            "TtC",
            [0, 1, 10, 11, 12],
            "FaIR1.6",
            "1pctCO2",
            region="World",
            model="unspecified",
        )
    )
    expected_metrics.append(
        _create_expected_metric_df(
            scmrun[-1],
            "Transient Climate Response to Emissions",
            "K / TtC",
            (
                (0, 2 / 1),
                (1, 1.7 / 1.1),
                (10, 1.3 / 1.2),
                (11, 0.9 / 1.3),
                (12, 0.8 / 1.4),
            ),
            norm_period=(1850, 1850),
            eval_period=(1920, 1920),
            variable="Surface Air Temperature Change,Cumulative Emissions|CO2",
        )
    )

    scmrun.append(
        _create_scm_run(
            np.vstack(
                [
                    # add one to ensure no evaluation period is applied
                    2 * np.ones(251),
                    2.1 * np.ones(251),
                    2.2 * np.ones(251),
                    2.3 * np.ones(251),
                    2.4 * np.ones(251),
                ]
            ).T,
            range(1850, 2100 + 1),
            "Atmospheric Lifetime|CH4",
            "month",
            [0, 1, 10, 11, 12],
            "FaIR1.6",
            # shouldn't get confused by extra scenarios
            scenario="historical",
            region="World",
            model="unspecified",
        )
    )
    expected_metrics.append(
        _create_expected_metric_df(
            scmrun[-1],
            "Atmospheric Lifetime|CH4 World historical-2005",
            "yr",
            (
                (0, 2 / 12),
                (1, 2.1 / 12),
                (10, 2.2 / 12),
                (11, 2.3 / 12),
                (12, 2.4 / 12),
            ),
            eval_period=(2005, 2014),
        )
    )

    scmrun.append(
        _create_scm_run(
            np.vstack(
                [
                    1.34 * np.arange(251) / 70
                    + np.sin(np.arange(251) * 2 * np.pi / 70),
                    1.18 * np.arange(251) / 70
                    + np.sin(np.arange(251) * 2 * np.pi / 70),
                ]
            ).T,
            range(1850, 2100 + 1),
            "Surface Air Temperature Change",
            "K",
            [0, 1],
            "MCE",
            "1pctCO2",
            region="World",
            model="unspecified",
        )
    )
    expected_metrics.append(
        _create_expected_metric_df(
            scmrun[-1],
            "Transient Climate Response",
            "K",
            ((0, 1.34), (1, 1.18)),
            norm_period=(1850, 1850),
            eval_period=(1920, 1920),
        )
    )

    scmrun.append(
        _create_scm_run(
            np.vstack([1000 * np.arange(251) / 70, 900 * np.arange(251) / 70]).T,
            range(1850, 2100 + 1),
            "Cumulative Emissions|CO2",
            "Mt CO2",
            [0, 1],
            "MCE",
            "1pctCO2",
            region="World",
            model="unspecified",
        )
    )
    expected_metrics.append(
        _create_expected_metric_df(
            scmrun[-1],
            "Transient Climate Response to Emissions",
            "K / TtC",
            ((0, 1.34 / 1000 * 10 ** 6 * 44 / 12), (1, 1.18 / 900 * 10 ** 6 * 44 / 12)),
            norm_period=(1850, 1850),
            eval_period=(1920, 1920),
            variable="Surface Air Temperature Change,Cumulative Emissions|CO2",
        )
    )

    scmrun.append(
        _create_scm_run(
            278 + np.vstack([np.arange(251), 1.02 * np.arange(251)]).T,
            range(1850, 2100 + 1),
            "Atmospheric Concentrations|CO2",
            "ppm",
            [0, 1],
            "MCE",
            "esm-hist",
            region="World",
            model="unspecified",
        )
    )
    expected_metrics.append(
        _create_expected_metric_df(
            scmrun[-1],
            "Rate Increase Atmospheric Concentrations|CO2 World esm-hist-1980",
            "ppm / yr",
            ((0, 1), (1, 1.02)),
            eval_period=(1980, 1989),
        )
    )

    scmrun = scmdata.run_append(scmrun)
    expected_metrics = pd.concat(expected_metrics).reset_index(drop=True)

    return scmrun, expected_metrics


@pytest.mark.parametrize(
    "metric",
    (
        "Transient Climate Response",
        "Transient Climate Response to Emissions",
        "Atmospheric Lifetime|CH4 World historical-2005",
        "Heat Content|Ocean World ssp245 1971-2018",
        "Rate Increase Atmospheric Concentrations|CO2 World esm-hist-1980",
    ),
)
def test_calculate_metric_from_results(
    metric, assessed_ranges_sample, scmrun_results_sample_expected_metrics
):
    res_calc, expected_all_metrics = scmrun_results_sample_expected_metrics

    exp = expected_all_metrics.loc[
        expected_all_metrics[assessed_ranges_sample.metric_column] == metric, :
    ].dropna(how="all", axis="columns")

    tcustom_calculators = (
        pyrcmip.metric_calculations.CalculatorTCR,
        pyrcmip.metric_calculations.CalculatorTCRE,
    )

    res = assessed_ranges_sample.calculate_metric_from_results(
        metric, res_calc, custom_calculators=tcustom_calculators,
    )

    pdt.assert_frame_equal(
        res.sort_values(["climate_model", "ensemble_member"]).reset_index(drop=True),
        exp.sort_values(["climate_model", "ensemble_member"]).reset_index(drop=True),
        check_like=True,
        check_dtype=False,
        rtol=1e-4,
    )


def test_calculate_metric_from_results_no_data(
    assessed_ranges_sample, scmrun_results_sample_expected_metrics
):
    res_calc, _ = scmrun_results_sample_expected_metrics
    res_calc = res_calc.filter(scenario="1pctCO2", keep=False)

    error_msg = re.escape(
        "Transient Climate Response requires "
        "{'variable': ['Surface Air Temperature Change'], "
        "'region': ['World'], 'scenario': ['1pctCO2']} but it is "
        "not in the results"
    )
    with pytest.raises(ValueError, match=error_msg):
        assessed_ranges_sample.calculate_metric_from_results(
            "Transient Climate Response", res_calc
        )


@pytest.mark.parametrize("climate_models", (["*"], ["MAGICC6", "FaIR1.6"]))
@patch("pyrcmip.assessed_ranges.plt")
@patch("pyrcmip.assessed_ranges.run_append")
def test_plot_against_results(
    mock_run_append,
    mock_plt,
    climate_models,
    assessed_ranges_sample_df,
    scmrun_results_sample_expected_metrics,
):
    results, expected_all_metrics = scmrun_results_sample_expected_metrics

    tmetrics = expected_all_metrics["RCMIP name"].unique()
    tdf = assessed_ranges_sample_df.loc[
        assessed_ranges_sample_df["RCMIP name"].isin(tmetrics), :
    ]
    ars = AssessedRanges(tdf)

    results_database = MagicMock()

    results = results.filter(climate_model=climate_models)

    mock_run_append.return_value = results

    tcustom_calculators = (
        pyrcmip.metric_calculations.CalculatorTCR,
        pyrcmip.metric_calculations.CalculatorTCRE,
    )
    tpalette = {
        "MAGICC6": "a",
        "FaIR1.6": "b",
        "MCE": "c",
    }

    with patch.object(ars, "plot_metric_and_results") as mock_plot_metric_and_results:

        with patch.object(
            ars, "plot_metric_and_results_box_only"
        ) as mock_plot_metric_and_results_box_only:

            res = ars.plot_against_results(
                results_database,
                custom_calculators=tcustom_calculators,
                climate_models=climate_models,
                palette=tpalette,
            )

    exp = []
    for metric in tmetrics:
        derived = ars.calculate_metric_from_results(
            metric, results, custom_calculators=tcustom_calculators
        )
        summary_table = ars.get_results_summary_table_for_metric(metric, derived)
        exp.append(summary_table)

    exp = pd.concat(exp)
    pdt.assert_frame_equal(
        res.sort_values(
            ["climate_model", "assessed_range_label", "RCMIP name"]
        ).reset_index(drop=True),
        exp.sort_values(
            ["climate_model", "assessed_range_label", "RCMIP name"]
        ).reset_index(drop=True),
        rtol=1e-4,
        atol=1e-4,
        check_like=True,
    )

    database_load_data_calls = [
        [
            call(climate_model=cm, variable=v, region=r, scenario=s)
            for cm in climate_models
            for v in ["Surface Air Temperature Change"]
            for r in ["World"]
            for s in ["1pctCO2"]
        ],
        [
            call(climate_model=cm, variable=v, region=r, scenario=s)
            for cm in climate_models
            for v in ["Surface Air Temperature Change", "Cumulative Emissions|CO2"]
            for r in ["World"]
            for s in ["1pctCO2"]
        ],
        [
            call(climate_model=cm, variable=v, region=r, scenario=s)
            for cm in climate_models
            for v in ["Atmospheric Concentrations|CO2"]
            for r in ["World"]
            for s in ["esm-hist"]
        ],
        [
            call(climate_model=cm, variable=v, region=r, scenario=s)
            for cm in climate_models
            for v in ["Atmospheric Lifetime|CH4"]
            for r in ["World"]
            for s in ["historical"]
        ],
        [
            call(climate_model=cm, variable=v, region=r, scenario=s)
            for cm in climate_models
            for v in ["Heat Content|Ocean"]
            for r in ["World"]
            for s in ["ssp245"]
        ],
    ]
    database_load_data_calls = [v for s in database_load_data_calls for v in s]

    results_database.load_data.assert_has_calls(database_load_data_calls)
    assert mock_run_append.call_count == len(tmetrics)

    for call_val in mock_plot_metric_and_results.call_args_list:
        kwargs = call_val[1]
        assert kwargs["palette"] == tpalette

    for call_val in mock_plot_metric_and_results_box_only.call_args_list:
        kwargs = call_val[1]
        assert kwargs["palette"] == tpalette


def test_plot_against_results_no_evaluation_method_warning(assessed_ranges_sample_df):
    no_method_metrics = assessed_ranges_sample_df.loc[
        assessed_ranges_sample_df["RCMIP evaluation method"].isnull(), :
    ]
    ars = AssessedRanges(no_method_metrics)

    warn_msg = re.escape("No evaluation method for No method metric")
    with pytest.warns(UserWarning, match=warn_msg):
        with pytest.raises(ValueError, match="No objects to concatenate"):
            ars.plot_against_results("not used")


@patch("pyrcmip.assessed_ranges.plt")
def test_plot_model_reported_against_assessed_ranges(
    mock_plt, assessed_ranges_sample_df, scmrun_results_sample_expected_metrics
):
    results, expected_all_metrics = scmrun_results_sample_expected_metrics

    tmetrics = [
        "Transient Climate Response",
        "Atmospheric Lifetime|CH4 World historical-2005",
    ]
    tdf = assessed_ranges_sample_df.loc[
        assessed_ranges_sample_df["RCMIP name"].isin(tmetrics), :
    ]
    ars = AssessedRanges(tdf)

    model_reported = []
    for metric in tmetrics:
        model_reported.append(
            ars.calculate_metric_from_results(
                metric,
                results,
                custom_calculators=(pyrcmip.metric_calculations.CalculatorTCR,),
            )
        )

    model_reported = pd.concat(model_reported)

    tpalette = {
        "MAGICC6": "a",
        "FaIR1.6": "b",
        "MCE": "c",
    }

    with patch.object(ars, "plot_metric_and_results") as mock_plot_metric_and_results:

        with patch.object(
            ars, "plot_metric_and_results_box_only"
        ) as mock_plot_metric_and_results_box_only:

            res = ars.plot_model_reported_against_assessed_ranges(
                model_reported, palette=tpalette,
            )

    exp = []
    for metric in tmetrics:
        summary_table = ars.get_results_summary_table_for_metric(metric, model_reported)
        exp.append(summary_table)

    exp = pd.concat(exp)

    pdt.assert_frame_equal(
        res.sort_values(
            ["climate_model", "assessed_range_label", "RCMIP name"]
        ).reset_index(drop=True),
        exp.sort_values(
            ["climate_model", "assessed_range_label", "RCMIP name"]
        ).reset_index(drop=True),
        rtol=1e-5,
        check_like=True,
    )

    for call_val in mock_plot_metric_and_results.call_args_list:
        kwargs = call_val[1]
        assert kwargs["palette"] == tpalette

    for call_val in mock_plot_metric_and_results_box_only.call_args_list:
        kwargs = call_val[1]
        assert kwargs["palette"] == tpalette
