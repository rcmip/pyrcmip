import os.path

import pytest
from scmdata.testing import assert_scmdf_almost_equal

from pyrcmip.io import (
    read_results_submission,
    read_submission_model_metadata,
    read_submission_model_reported,
)
from pyrcmip.validate import (
    validate_submission,
    validate_submission_model_meta,
    validate_submission_model_reported_metrics,
)


@pytest.fixture
def test_model_reported_csv(test_data_dir):
    return os.path.join(test_data_dir, "rcmip_model_reported_metrics_test.csv")


def test_read_submission_model_reported(test_model_reported_csv):
    res = read_submission_model_reported(test_model_reported_csv)

    # results are valid and pass validation
    validate_submission_model_reported_metrics(res)


@pytest.fixture
def test_model_metadata_csv(test_data_dir):
    return os.path.join(test_data_dir, "rcmip_model_metadata_test.csv")


def test_read_submission_model_metadata(test_model_metadata_csv):
    res = read_submission_model_metadata(test_model_metadata_csv)

    # results are valid and pass validation
    validate_submission_model_meta(res)


@pytest.fixture
def test_model_metadata_csv_from_excel(test_data_dir):
    return os.path.join(test_data_dir, "rcmip-model-meta-test.csv")


def test_read_submission_model_metadata_from_excel(test_model_metadata_csv_from_excel):
    res = read_submission_model_metadata(test_model_metadata_csv_from_excel)

    # results are valid and pass validation
    validate_submission_model_meta(res)


@pytest.mark.parametrize(
    "input_filename",
    (
        "rcmip_model_output_test.csv",
        "rcmip_model_output_test.csv.gz",
        "rcmip_model_output_test.xlsx",
        "rcmip_model_output_test.nc",
        ["rcmip_model_output_test_ssps.nc", "rcmip_model_output_test_nonssps.nc"],
        ["rcmip_model_output_test_nonssps.nc", "rcmip_model_output_test_ssps.nc"],
    ),
)
def test_read_results_submission(test_data_dir, input_filename):
    if isinstance(input_filename, list):
        test_results = [os.path.join(test_data_dir, f) for f in input_filename]
    else:
        test_results = os.path.join(test_data_dir, input_filename)
    res = read_results_submission(test_results)

    # results are valid and pass validation
    validate_submission(res)


def test_read_results_submission_multiple(test_data_dir):
    split_fnames = [
        os.path.join(test_data_dir, "rcmip_model_output_test_nonssps.nc"),
        os.path.join(test_data_dir, "rcmip_model_output_test_ssps.nc"),
    ]
    split_res = read_results_submission(split_fnames)

    merged_res = read_results_submission(
        os.path.join(test_data_dir, "rcmip_model_output_test.nc")
    )

    assert_scmdf_almost_equal(split_res, merged_res, check_ts_names=False)
