import logging
import os.path
import re

import numpy as np
import pandas.testing as pdt
import pytest
from scmdata import ScmRun
from scmdata.run import BaseScmRun

from pyrcmip.errors import ProtocolConsistencyError
from pyrcmip.io import (
    read_results_submission,
    read_submission_model_metadata,
    read_submission_model_reported,
)
from pyrcmip.validate import validate_submission, validate_submission_bundle


@pytest.fixture
def start_scmrun():
    return ScmRun(
        data=np.arange(6).reshape(3, 2),
        index=range(2000, 2021, 10),
        columns={
            "scenario": ["ssp119", "historical"],
            "model": "unspecified",
            "variable": "Surface Air Temperature Change",
            "region": "World",
            "unit": "K",
            "climate_model": "test_model v1.2.3",
            "ensemble_member": [0, 0],
        },
    )


def test_validate_submission(submission_template_xlsx, start_scmrun, caplog):
    # no error raised
    with caplog.at_level(logging.INFO):
        validate_submission(start_scmrun, submission_template_xlsx)

    assert caplog.records[-1].msg == "valid submission"


@pytest.mark.parametrize(
    "bad_col, bad_values, msg, expected_total_errors",
    (
        (
            "scenario",
            ["ssp119", "Historical"],
            "problems with scenarios in submission",
            1,
        ),
        (
            "variable",
            ["Emissions|CO2", "Emissions|Co2"],
            "problems with variables in submission",
            2,
        ),  # extra error because unit conversion also doesn't work
        ("region", ["World", "Global"], "problems with regions in submission", 1),
        ("unit", ["t", "t"], "problems converting units in submission", 1),
    ),
)
def test_validate_submission_bad_col(
    submission_template_xlsx,
    start_scmrun,
    bad_col,
    bad_values,
    msg,
    caplog,
    expected_total_errors,
):
    start_scmrun[bad_col] = bad_values

    error_msg = "submission is inconsistent with the RCMIP protocol"
    with caplog.at_level(logging.WARNING):
        with pytest.raises(ProtocolConsistencyError, match=error_msg):
            validate_submission(start_scmrun, submission_template_xlsx)

    assert len(caplog.records) == expected_total_errors
    assert caplog.records[0].msg == msg
    assert caplog.records[0].levelname == "ERROR"


def test_validate_submission_bad_columns(
    submission_template_xlsx, start_scmrun, caplog
):
    start_scmrun["scenario"] = ["SSp245", "Historical"]
    start_scmrun["variable"] = [
        "Effective Radiative Forcing",
        "Effective Radiative Forcing|Aerosols",
    ]
    start_scmrun["region"] = ["World", "GLOBAL"]
    start_scmrun["unit"] = ["W", "W/m^2"]
    start_scmrun = start_scmrun.drop_meta("climate_model", inplace=False)

    error_msg = "submission is inconsistent with the RCMIP protocol"
    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_submission(start_scmrun, submission_template_xlsx)

    assert len(caplog.records) == 5

    assert caplog.records[0].msg == "problems with variables in submission"
    assert caplog.records[0].levelname == "ERROR"

    assert caplog.records[1].msg == "problems with regions in submission"
    assert caplog.records[1].levelname == "ERROR"

    assert caplog.records[2].msg == "problems with scenarios in submission"
    assert caplog.records[2].levelname == "ERROR"

    assert caplog.records[3].msg == "no climate_model metadata included in submission"
    assert caplog.records[3].levelname == "ERROR"

    assert caplog.records[4].msg == "problems converting units in submission"
    assert caplog.records[4].levelname == "ERROR"


@pytest.mark.parametrize("missing_col", ("climate_model", "ensemble_member"))
def test_validate_submission_missing_column(
    missing_col, submission_template_xlsx, start_scmrun, caplog
):
    start_scmrun = start_scmrun.drop_meta(missing_col, inplace=False)

    error_msg = "submission is inconsistent with the RCMIP protocol"
    with caplog.at_level(logging.WARNING):
        with pytest.raises(ProtocolConsistencyError, match=error_msg):
            validate_submission(start_scmrun, submission_template_xlsx)

    assert len(caplog.records) == 1

    assert caplog.records[0].msg == "no {} metadata included in submission".format(
        missing_col
    )
    assert caplog.records[0].levelname == "ERROR"


def test_validate_submission_no_unit_column(
    submission_template_xlsx, start_scmrun, caplog
):
    class NoUnitScmRun(BaseScmRun):
        required_cols = ("variable",)

    start_scmrun = NoUnitScmRun(
        start_scmrun.timeseries(list(set(start_scmrun.meta.columns) - {"unit"}))
    )

    error_msg = "submission is inconsistent with the RCMIP protocol"
    with caplog.at_level(logging.WARNING):
        with pytest.raises(ProtocolConsistencyError, match=error_msg):
            validate_submission(start_scmrun, submission_template_xlsx)

    assert len(caplog.records) == 1

    assert caplog.records[0].msg == "no unit metadata included in submission"
    assert caplog.records[0].levelname == "ERROR"


@pytest.fixture
def valid_bundle(test_data_dir):
    return (
        read_results_submission(
            os.path.join(test_data_dir, "rcmip_model_output_test.nc")
        ),
        read_submission_model_reported(
            os.path.join(test_data_dir, "rcmip_model_reported_metrics_test.csv")
        ),
        read_submission_model_metadata(
            os.path.join(test_data_dir, "rcmip_model_metadata_test.csv")
        ),
    )


def test_validate_submission_bundle(valid_bundle, caplog):
    timeseries, model_reported, metadata = valid_bundle

    with caplog.at_level(logging.INFO):
        res = validate_submission_bundle(timeseries, model_reported, metadata)

    pdt.assert_frame_equal(
        res[0].timeseries().sort_index(),
        timeseries.timeseries(res[0].meta.columns).sort_index(),
    )
    pdt.assert_frame_equal(res[1], model_reported)
    pdt.assert_frame_equal(res[2], metadata)

    assert any([v.msg == "valid timeseries" for v in caplog.records])
    assert any([v.msg == "valid metadata" for v in caplog.records])
    assert any([v.msg == "valid model reported csv" for v in caplog.records])
    assert caplog.records[-1].msg == "valid submission bundle"


def test_validate_submission_bundle_non_matching_climate_model_in_timeseries(
    valid_bundle, caplog
):
    timeseries, model_reported, metadata = valid_bundle
    timeseries["climate_model"] = metadata["climate_model"].unique()[0] + "a"

    error_msg = re.escape("climate_model not consistent across the bundle")
    with caplog.at_level(logging.INFO):
        with pytest.raises(ValueError, match=error_msg):
            validate_submission_bundle(timeseries, model_reported, metadata)

    assert any(
        [
            v.msg
            == "climate_model(s) `{'two_layera'}` in timeseries is not found in metadata"
            for v in caplog.records
        ]
    )


def test_validate_submission_bundle_non_matching_climate_model_in_model_reported(
    valid_bundle, caplog
):
    timeseries, model_reported, metadata = valid_bundle
    model_reported["climate_model"] = metadata["climate_model"].unique()[0] + "a"

    error_msg = re.escape("climate_model not consistent across the bundle")
    with caplog.at_level(logging.INFO):
        with pytest.raises(ValueError, match=error_msg):
            validate_submission_bundle(timeseries, model_reported, metadata)

    assert any(
        [
            v.msg
            == "climate_model(s) `{'two_layera'}` in model reported csv is not found in metadata"
            for v in caplog.records
        ]
    )


def test_validate_submission_bundle_bad_unit(valid_bundle, caplog):
    timeseries, model_reported, metadata = valid_bundle

    timeseries["unit"] = "junk"

    error_msg = re.escape("submission bundle is inconsistent with the RCMIP protocol")
    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_submission_bundle(timeseries, model_reported, metadata)

    assert any(
        [v.msg == "problems converting units in submission" for v in caplog.records]
    )


def test_validate_submission_bundle_bad_meta(valid_bundle, caplog):
    timeseries, model_reported, metadata = valid_bundle

    metadata["extra_col"] = "hi"

    error_msg = re.escape("submission bundle is inconsistent with the RCMIP protocol")
    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_submission_bundle(timeseries, model_reported, metadata)

    assert any(
        [v.msg == "problems with metadata in submission" for v in caplog.records]
    )


def test_validate_submission_bundle_bad_model_reported(valid_bundle, caplog):
    timeseries, model_reported, metadata = valid_bundle

    model_reported["extra_col"] = "extra"

    error_msg = re.escape("submission bundle is inconsistent with the RCMIP protocol")
    with pytest.raises(ProtocolConsistencyError, match=error_msg):
        validate_submission_bundle(timeseries, model_reported, metadata)

    assert any(
        [
            v.msg == "problems with model reported csv in submission"
            for v in caplog.records
        ]
    )
