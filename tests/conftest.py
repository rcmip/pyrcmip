from os.path import dirname, join

import boto3
import jwt
import pandas as pd
import pytest
from click.testing import CliRunner
from moto import mock_s3
from scmdata import ScmRun

from pyrcmip import cli
from pyrcmip.assessed_ranges import AssessedRanges

TEST_DATA_DIR = join(dirname(__file__), "data")
TEST_RESULTS_DIR = join(TEST_DATA_DIR, "results", "phase-1")


def get_upload_key(org, model, version, ending):
    return join(org, model, version, "rcmip-{}-{}-{}".format(model, version, ending))


def check_file_uploaded(conn, bucket_name, org, model, version, ending):
    assert (
        conn.Object(bucket_name, get_upload_key(org, model, version, ending)).get()
        is not None
    )


@pytest.fixture(scope="module")
def test_data_dir():
    return TEST_DATA_DIR


@pytest.fixture(scope="module")
def results_dir():
    return TEST_RESULTS_DIR


@pytest.fixture(scope="module")
def submission_template_xlsx():
    return join(TEST_DATA_DIR, "rcmip-data-submission-template-test.xlsx")


@pytest.fixture(scope="module")
def phase_1_results():
    return ScmRun(
        join(
            TEST_DATA_DIR,
            "results",
            "phase-1",
            "magicc7/rcmip_phase-1_magicc7.1.0.beta-ukesm1-0-ll-r1i1p1f2_v1-0-0.csv",
        )
    )


@pytest.fixture()
def run_cli():
    def f(args):
        runner = CliRunner()
        return runner.invoke(cli.cli, args)

    return f


@pytest.fixture(scope="function")
def bucket():
    with mock_s3():
        bucket_name = "test-bucket"

        conn = boto3.resource("s3")
        conn.create_bucket(Bucket=bucket_name)

        yield conn, bucket_name


@pytest.fixture()
def run_upload():
    def f(data=None, **kwargs):
        kwargs.setdefault(
            "token",
            jwt.encode(
                {
                    "access_key_id": "test_access_key",
                    "secret_access_key": "test_secret_key",
                    "org": "myOrg",
                },
                "secret",
            ),
        )
        kwargs.setdefault("model", "magicc")
        kwargs.setdefault("version", "2.0.0")

        args = ["upload"]
        for k in kwargs:
            args.extend(["--" + k, kwargs[k]])

        if data:
            args.extend(data)
        else:
            args += [
                join(TEST_DATA_DIR, "rcmip_model_output_test.nc"),
                join(TEST_DATA_DIR, "rcmip_model_reported_metrics_test.csv"),
                join(TEST_DATA_DIR, "rcmip_model_metadata_test.csv"),
            ]

        runner = CliRunner()
        return runner.invoke(cli.cli, args)

    return f


@pytest.fixture()
def run_download(tmpdir):
    def f(**kwargs):
        kwargs.setdefault(
            "token",
            jwt.encode(
                {
                    "access_key_id": "test_access_key",
                    "secret_access_key": "test_secret_key",
                    "org": "myOrg",
                },
                "secret",
            ),
        )
        kwargs.setdefault("model", "magicc")
        kwargs.setdefault("version", "2.0.0")

        args = ["download"]
        for k in kwargs:
            args.extend(["--" + k, kwargs[k]])
        args.append(str(tmpdir))

        runner = CliRunner()
        return runner.invoke(cli.cli, args)

    return f, str(tmpdir)


@pytest.fixture()
def assessed_ranges_sample_df(test_data_dir):
    return pd.read_csv(join(test_data_dir, "rcmip-assessed-ranges-v2-1-2.csv"))


@pytest.fixture()
def assessed_ranges_sample(assessed_ranges_sample_df):
    return AssessedRanges(assessed_ranges_sample_df)


@pytest.fixture()
def valid_model_reported():
    return pd.DataFrame(
        [
            {
                "unit": "K",
                "value": 1.5,
                "ensemble_member": 0,
                "climate_model": "two_layer",
                "RCMIP name": "Equilibrium Climate Sensitivity",
            },
            {
                "unit": "K",
                "value": 2.0,
                "ensemble_member": 1,
                "climate_model": "two_layer",
                "RCMIP name": "Equilibrium Climate Sensitivity",
            },
            {
                "unit": "K",
                "value": 2.5,
                "ensemble_member": 2,
                "climate_model": "two_layer",
                "RCMIP name": "Equilibrium Climate Sensitivity",
            },
        ]
    )
